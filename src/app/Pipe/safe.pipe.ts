import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import * as moment from 'moment';

@Pipe({
  name: 'safe'
})
export class SafePipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) { }
  transform(resumeLink) {
    return this.sanitizer.bypassSecurityTrustResourceUrl(resumeLink);
  }
}

@Pipe({
  name: 'calendar'
})
export class CalendarPipe implements PipeTransform {

  constructor() { }

  transform(calendarData): string {
    /* Set Date for Calender */
    let tempDate = new Date(calendarData['datepicker'])
    if (calendarData['dpHour'] == '12') {
      if (calendarData['dpFormat'] == 'AM')
        tempDate.setHours(0)
      else tempDate.setHours(12)
    } else if (calendarData['dpFormat'] == 'AM')
      tempDate.setHours(parseInt(calendarData['dpHour']))
    else
      tempDate.setHours(parseInt(calendarData['dpHour']) + 12)

    tempDate.setMinutes(parseInt(calendarData['dpMinute']))
    /* Set Date for Calender */

    return moment(tempDate).format('YYYY-MM-DDTHH:mm');
  }
}
