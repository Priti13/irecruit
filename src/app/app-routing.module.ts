import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './_modules/admin/login/login.component';
import { DashboardComponent } from './_modules/admin/dashboard/dashboard.component';
import { ViewErfComponent } from './_modules/admin/view-erf/view-erf.component';
import { GenerateErfComponent } from './_modules/admin/generate-erf/generate-erf.component';
import { AuthGuard } from './_helpers/auth.guard';
import { ErfdetailComponent } from './_modules/admin/erfdetail/erfdetail.component';
import { UploadComponent } from './_modules/admin/upload/upload.component';
import { AssignRecruiterComponent } from './_modules/admin/assign-recruiter/assign-recruiter.component';
import { AssignRecruiterDetailComponent } from './_modules/admin/assign-recruiter-detail/assign-recruiter-detail.component';
import { AssignResumeComponent } from './_modules/admin/assign-resume/assign-resume.component';
import { SetPanelComponent } from './_modules/admin/set-panel/set-panel.component';
import { SetCalendarComponent } from './_modules/admin/set-calendar/set-calendar.component';
import { FeedbackComponent } from './_modules/admin/feedback/feedback.component';
import { JoiningDataComponent } from './_modules/admin/joining-data/joining-data.component';
import { JobDescriptionComponent } from './_modules/admin/job-description/job-description.component';
import { OffersComponent } from './_modules/admin/offers/offers.component';
import { ViewResumeComponent } from './_modules/admin/view-resume/view-resume.component';
import { ResumeDetailsComponent } from './_modules/admin/resume-details/resume-details.component';
import { ResumeActionComponent } from './_modules/admin/resume-action/resume-action.component';
import { AssignedResumeComponent } from './_modules/admin/assigned-resume/assigned-resume.component';
import { FeedbackViewComponent } from './_modules/admin/feedback-view/feedback-view.component';
import { ProfileComponent } from './_modules/admin/profile/profile.component';
import { SettingComponent } from './_modules/admin/setting/setting.component';
import { ChangePasswordComponent } from './_modules/admin/change-password/change-password.component';
import { CreatePermissionComponent } from './_modules/admin/create-permission/create-permission.component';
import { ForgotPasswordComponent } from './_modules/admin/forgot-password/forgot-password.component';
import { SetpermissionComponent } from './_modules/admin/setpermission/setpermission.component';
import { ReportComponent } from './_modules/admin/report/report.component';
import { SetFeedbackComponent } from './_modules/admin/set-feedback/set-feedback.component';
import { AdminSettingComponent } from './_modules/admin/admin-setting/admin-setting.component';
import { EmployeeDetailComponent } from './_modules/admin/employee-detail/employee-detail.component';
import { SetemployeeDetailComponent } from './_modules/admin/setemployee-detail/setemployee-detail.component';
import { RasDetailComponent } from './_modules/admin/ras-detail/ras-detail.component';
import { RasDetailViewComponent } from './_modules/admin/ras-detail-view/ras-detail-view.component';
import { RasReportComponent } from './_modules/admin/ras-report/ras-report.component';
import { RasReportDetailComponent } from './_modules/admin/ras-report-detail/ras-report-detail.component';
import {ExternalfeedbackComponent} from './_modules/admin/externalfeedback/externalfeedback.component';
import {ThemeSettingComponent} from './_modules/admin/theme-setting/theme-setting.component'

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
  { path: 'view-project', component: ViewErfComponent, canActivate: [AuthGuard] },
  { path: 'generate-project', component: GenerateErfComponent, canActivate: [AuthGuard] },
  { path: 'detail-project/:id', component: ErfdetailComponent, canActivate: [AuthGuard] },
  { path: 'upload-resume', component: UploadComponent, canActivate: [AuthGuard] },
  { path: 'assign-recruiter', component: AssignRecruiterComponent, canActivate: [AuthGuard] },
  { path: 'assign-recruiter-detail/:id', component: AssignRecruiterDetailComponent, canActivate: [AuthGuard] },
  { path: 'assign-resume', component: AssignResumeComponent, canActivate: [AuthGuard] },
  { path: 'search-resumes/:id', component: ResumeDetailsComponent, canActivate: [AuthGuard] },
  { path: 'resume-action', component: ResumeActionComponent, canActivate: [AuthGuard] },
  { path: 'job-description', component: JobDescriptionComponent, canActivate: [AuthGuard] },
  { path: 'assigned-resume/:id', component: AssignedResumeComponent, canActivate: [AuthGuard] },
  { path: 'view-resume/:id', component: ViewResumeComponent, canActivate: [AuthGuard] },
  { path: 'set-panel/:jdId/:resumeId', component: SetPanelComponent, canActivate: [AuthGuard] },
  { path: 'set-calendar/:jdId/:resumeId', component: SetCalendarComponent, canActivate: [AuthGuard] },
  { path: 'set-feedback/:jdId/:resumeId', component: SetFeedbackComponent, canActivate: [AuthGuard] },
  { path: 'feedback/:jdId/:resumeId/:interviewId', component: FeedbackComponent, canActivate: [AuthGuard] },
  { path: 'feedback-view/:jdId/:resumeId/:interviewId', component: FeedbackViewComponent, canActivate: [AuthGuard] },
  { path: 'offers', component: OffersComponent, canActivate: [AuthGuard] },
  { path: 'joining-data/:jdId/:resumeId', component: JoiningDataComponent, canActivate: [AuthGuard] },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
  { path: 'settings', component: SettingComponent, canActivate: [AuthGuard] },
  { path: 'change-password', component: ChangePasswordComponent, canActivate: [AuthGuard] },
  { path: 'forgot-password', component: ForgotPasswordComponent, canActivate: [AuthGuard] },
  { path: 'report', component: ReportComponent, canActivate: [AuthGuard] },
  { path: 'ras-detail', component: RasDetailComponent, canActivate: [AuthGuard] },
  { path: 'rasdetail-view', component: RasDetailViewComponent, canActivate: [AuthGuard] },
  { path: 'ras-report', component: RasReportComponent, canActivate: [AuthGuard] },
  { path: 'ras-report-detail', component: RasReportDetailComponent, canActivate: [AuthGuard] },
  { path: 'theme-setting', component: ThemeSettingComponent, canActivate: [AuthGuard] },
  { path: 'admin-settings', component: AdminSettingComponent, canActivate: [AuthGuard] },
  { path: 'emp-deatil', component: EmployeeDetailComponent, canActivate: [AuthGuard] },
  { path: 'setemp-detail/:id', component: SetemployeeDetailComponent, canActivate: [AuthGuard] },
  { path: 'create-permission', component: CreatePermissionComponent, canActivate: [AuthGuard] },
  { path: 'set-permission/:id', component: SetpermissionComponent, canActivate: [AuthGuard] },
  {path:'external-feedback/:token',component:ExternalfeedbackComponent},
  //
  // { path: 'assign-recruiter', component: AssignRecruiterComponent, canActivate: [AuthGuard] },

  // otherwise redirect to home
  { path: '**', redirectTo: 'login', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
