export class User {
  username: string = 'rudra@yopmail.com';
  password: string = '';
  // tslint:disable-next-line: variable-name
  grant_type: string = 'password';
}
export class RefreshUser {
  username: string;
  password: string;
  // tslint:disable-next-line: variable-name
  grant_type: string = 'refresh_token';
  refresh_token: string;
  constructor(username, password, refresh_token) {
    this.username = username
    this.password = password
    this.refresh_token = refresh_token
  }
}


export class CurrentUser {
  type: string;
  token: string;
  refresh: string;
  name: string;
  designation: string;
  department: string;
  email: string;
  sapId: string;
  id: number;
  permissions: any[];
  psw: string;
  isExpire: boolean;
}

export class GernerateErf {
  department: string;
  departmentHead: string;
  minExperience: number;
  maxExperience: number;
  position: number;
  requisitionCategory: string;
  project: string;
  budget: string;
  sourceCountry: string;
  sourceState: string;
  sourceZip: string;
  sourceAddress: string;
  workCountry: string;
  workState: string;
  workZip: string;
  workAddress: string;
  visaApplicable: boolean;
  billable: boolean;
}
export class GernerateErfObj {
  'project': any;
  'billable': boolean;
  "erfId": 0;
  "totalPosition": number;
  "requisitionCategory": "Manage Services";
  "resourceAllocation": any;
  "maxBudget": number;
  "sourceAddresses": [
    {
      "country": any,
      "state": any,
      "erf": null,
      "zipCode": number,
      "addressLineOne": ""
    }
  ];
  "workAddresses": [
    {
      "country": any,
      "state": any,
      "erf": null,
      "zipCode": number,
      "addressLineOne": ""
    }
  ];
  "workLocation": any;
  'currency': string;
}

export class UploadResume {
  firstName: string;
  lastName: string;
  address: string;
  city: string;
  state: string;
  zip: string;
  file: File;
}
