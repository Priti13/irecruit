import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { AlertService } from 'src/app/_service/alert.service';
import { UserService } from 'src/app/_service/user.service';


@Component({
  selector: 'app-theme-setting',
  templateUrl: './theme-setting.component.html',
  styleUrls: ['./theme-setting.component.css']
})
export class ThemeSettingComponent implements OnInit {
  themecolor: string;

  constructor(private userService: UserService, private alertService: AlertService,private spinner: NgxSpinnerService,) { }

  ngOnInit(): void {
    const color = localStorage.getItem('themeColor');
    if (color == null) {
      this.themecolor = '1';
    } else {
      this.themecolor = color;
    }

  }
  value(name: string, attr: string) {
    console.log("value", attr);
       this.spinner.show();
    this.userService.updateThemecolor(attr).subscribe(res => {
      this.spinner.hide();
      localStorage.setItem('themeColor', attr);
      window.location.reload();
      this.alertService.success('Successfully updated.');
    }
    ), error => {
      this.spinner.hide();
      this.alertService.error(error);
    }

  }

}
