import { Component, OnInit } from '@angular/core';
import { AssignRecruiterService } from 'src/app/_service/assign-recruiter.service';

import { from } from 'rxjs';
@Component({
  selector: 'app-create-permission',
  templateUrl: './create-permission.component.html',
  styleUrls: ['./create-permission.component.css']
})
export class CreatePermissionComponent implements OnInit {
  getpermission: any;
  empname: any;
  empid: any;

  constructor(private assignuserserv: AssignRecruiterService) { }

  ngOnInit() {
    this.createPermission();
  }
  createPermission() {
    return this.assignuserserv.createpermission().subscribe((res) => {
      this.getpermission = res
    });
  }
  employeeName(name, id) {
    this.empname = name;
    this.empid = id;
    this.assignuserserv.getEmpName(this.empname, this.empid);
  }

}
