import { Component, OnInit } from '@angular/core';
import { AssignRecruiterService } from 'src/app/_service/assign-recruiter.service';
import { ERFService } from 'src/app/_service/erf.service';
import { ActivatedRoute } from '@angular/router';
import { AlertService } from 'src/app/_service/alert.service';
import { JdService } from 'src/app/_service/jd.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CalendarPipe } from 'src/app/Pipe/safe.pipe';
import {AuthenticationService} from 'src/app/_service/authentication.service';
declare var $: any;

@Component({
  selector: 'app-set-calendar',
  templateUrl: './set-calendar.component.html',
  styleUrls: ['./set-calendar.component.css']
})
export class SetCalendarComponent implements OnInit {
  jdId: string;
  resumeId: string;
  setPanelData: any[] = [];
  round = 0;
  calendarData: FormGroup;
  submitted = false;
  panelId: number = 0;
  timeZone: any[];
  //shantam
  CALENDARSET: any;
CALENDARReschedule: any;
permissionData:any;

  constructor(private assignRecruiterService: AssignRecruiterService, private calendar: CalendarPipe,
    private alertService: AlertService, private erfService: ERFService,
    private jdService: JdService, private route: ActivatedRoute, private formBuilder: FormBuilder,
     private authserv:AuthenticationService) { }
  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.resumeId = params.get('resumeId');
      this.jdId = params.get('jdId');
      this.getSetPanelData(this.jdId, this.resumeId);
      this.getTimeZone();
      this.initMethod();
    });
    this.setpermission();
  }
  initMethod() {
    this.calendarData = this.formBuilder.group({
      datepicker: [new Date(), Validators.required],
      dpHour: ['', Validators.required],
      dpMinute: ['', Validators.required],
      dpFormat: ['', Validators.required],
      timeZoneId: ['', Validators.required],
    });
  }
  // convenience getter for easy access to form fields
  get f() { return this.calendarData.controls; }
  getSetPanelData(id, resumeid) {
    this.jdService.getsetPanel(id, resumeid).subscribe(res => {
      if (res) {
        this.setPanelData = res;
        const len = this.setPanelData.length - 1;
        this.round = this.setPanelData[len].round;
      }
    }, err => {
      this.alertService.error(err);
    });
  }
  getTimeZone() {
    this.assignRecruiterService.getTimeZone().subscribe(
      res => {
        this.timeZone = res;
        //console.log(this.timeZone);
      }, err => {
        this.alertService.error(err);
      }
    )
  }
  openSetCalendar(id) {
    this.panelId = id;
    
  }
  setCalendarDate() {
    for (var i = 0; i < this.timeZone.length; i++) {
      if (this.timeZone[i].name == this.calendarData.value.timeZoneId) {
        this.calendarData.value.timeZoneId = this.timeZone[i].id
        //
        this.submitted = true;
        if (this.calendarData.invalid) return;
        const dateInFormat = this.calendar.transform(this.calendarData.value)
        this.assignRecruiterService.setCalendar(this.panelId, dateInFormat, this.calendarData.value['timeZoneId']).subscribe(
          res => {
            this.alertService.success('Added successfully.');
            this.getSetPanelData(this.jdId, this.resumeId);
            $("#addCalendar").modal("hide");
            this.submitted = false;
            this.initMethod();
          }, err => {
            this.alertService.error(err);
          }
        )
        //
      }
      else {
        this.alertService.error("No Data Found");
      }
    }
  }


  setpermission(){
    this.permissionData= this.authserv.receive();
    if(this.permissionData===undefined){
      this.authserv.setpermission2().subscribe((res)=>{
       this.permissionData =res;
       this.permissionData= this.authserv.send(this.permissionData);
     if(this.permissionData){
      this.CALENDARSET=this.permissionData.CALENDAR_SET;
      this.CALENDARReschedule=this.permissionData.CALENDAR_RESCHEDULE;
      console.log("if setcalender",this.permissionData);
     }})}
     else{
  
      this.CALENDARSET=this.permissionData['CALENDAR_SET'],
      this.CALENDARReschedule=this.permissionData.CALENDAR_RESCHEDULE,
      
      console.log(" else setcalender",this.permissionData);
    }
   /* return this.authserv.permission2().subscribe(res=>{
      this.CALENDARSET=res.CALENDAR_SET;
      this.CALENDARReschedule=res.CALENDAR_RESCHEDULE;
      console.log(this.CALENDARSET,this.CALENDARReschedule);
    });*/
  }
}
