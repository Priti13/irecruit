import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/_service/user.service';
import { AlertService } from 'src/app/_service/alert.service';
import { SharedService } from 'src/app/_service/shared.service';
import { NgForm } from '@angular/forms';
import * as moment from 'moment';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  isPushNotification: boolean;
  isEmailNotification: boolean;

  profileData: any;
  countryList: any[];
  OTPForm: { country: string, code: string, phnno: string };
  verifyForm: { verifyEnable: boolean; otpnum: string; isotpnum: boolean; isOptEnable: boolean };
  timerSec: number;
  interval: any;
  otpf: NgForm;
  profilePic: any;

  constructor(
    private userService: UserService, private alertService: AlertService,
    private shareService: SharedService,
  ) { }
  ngOnInit() {
    this.initMethod();
  }
  initMethod() {
    this.timerSec = 45;
    this.isPushNotification = false;
    this.isEmailNotification = false;
    this.verifyForm = { verifyEnable: false, otpnum: '', isotpnum: false, isOptEnable: false }
    this.OTPForm = { country: '', code: '+ 00', phnno: '' }

    this.userService.getUserProfile().subscribe(
      res => this.profileData = res
    ), error => {
      this.alertService.error(error);
    }
    this.shareService.getCountryList().subscribe(
      (res: any[]) => this.countryList = res
    ), error => {
      this.alertService.error(error);
    }
  }

  profilePhoto(pic) {
    console.log('pic');
    this.userService.getUserProfilePhoto(pic).subscribe(res => {
      this.alertService.success(res);
    }
    ), error => {
      this.alertService.error(error);
    }
  }

  onCountryChange() {
    this.OTPForm['code'] = this.OTPForm['country']['phonecode']
  }
  onSendOTP(otpf: NgForm) {
    
    if (this.interval) {
      clearInterval(this.interval)
      this.timerSec = 45;
    }
    this.userService.sendOTP(this.OTPForm['code'], this.OTPForm['phnno']).subscribe(
      res => {
        if (res['status']) {
          this.verifyForm.verifyEnable = true;
          this.verifyForm.isOptEnable = true;
          this.startTimer();
          this.alertService.success(res['message'])
          this.otpf = otpf;
        } else {
          this.alertService.error(res['message']);
        }
      }, error => {
        this.alertService.error(error);
      }
    )
  }
  startTimer() {
    this.interval = setInterval(() => {
      if (this.timerSec > 0) {
        this.timerSec--;
      } else {
        clearInterval(this.interval)
        this.verifyForm.isOptEnable = false;
      }
    }, 1000)
  }
  onVerifyOtp() {
    if (this.verifyForm.otpnum === '') {
      this.verifyForm.isotpnum = true;
      return;
    }
    else this.verifyForm.isotpnum = false;
    this.userService.verifyOTP(this.verifyForm['otpnum'], this.OTPForm['phnno']).subscribe(
      res => {
        if (res['status']) {
          this.verifyForm.otpnum = '';
          this.alertService.success(res['message']);
          this.verifyForm = { verifyEnable: false, otpnum: '', isotpnum: false, isOptEnable: false };
          this.OTPForm = { country: '', code: '+', phnno: '' };
          this.otpf.resetForm();
          this.verifyForm.otpnum = '';
        } else
          this.alertService.error(res['message']);
      }, err => {
        this.alertService.error(err);
      }
    )
  }
  showUserDetail() {
    var b = moment(this.profileData['dateOfBirth']);
    var a = moment(new Date());
    this.profileData['age'] = a.diff(b, 'years');
  }
  closeModel() {
    this.isEmailNotification = false
  }
}
