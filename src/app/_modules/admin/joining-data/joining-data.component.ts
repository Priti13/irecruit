import { Component, OnInit, Inject, LOCALE_ID } from '@angular/core';
import { OffersService } from 'src/app/_service/offers.service';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertService } from 'src/app/_service/alert.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { JdService } from 'src/app/_service/jd.service';
import { AuthenticationService } from 'src/app/_service/authentication.service';
import { formatDate } from '@angular/common';

declare var $: any;

@Component({
  selector: 'app-joining-data',
  templateUrl: './joining-data.component.html',
  styleUrls: ['./joining-data.component.css']
})
export class JoiningDataComponent implements OnInit {
  joinData: any;
  resumeId: any;
  id: any;
  result: any;
  addJoinForm: FormGroup;
  submitted = false;
  addJoidata: any = {};
  offerType: string = '';
  jdId: any;
  resumes: any[] = [];
  jd: any = '';
  salaryStructure: [];
  permissionData: any;
  OFFERCONTROL: any;
  offerRelease: any;
  joiningDataFeedbackView: any;
  joiinindDataJdView: any;
  JOININGDATAUPDATE: any;
  JOININGDATAVIEW: any;
  joiningId: any;
  approved_status: string = '';
  rejectReason: string;

  constructor(
    private offerService: OffersService, private route: ActivatedRoute,
    private frmbuilder: FormBuilder, private alertService: AlertService,
    private spinner: NgxSpinnerService, private jdService: JdService,
    private authserv: AuthenticationService, @Inject(LOCALE_ID) locale: string
  ) { }
  ngOnInit() {
    // this.spinner.show();
    this.route.paramMap.subscribe(params => {
      this.jdId = params.get('jdId');
      this.resumeId = params.get('resumeId');
      this.getJoinData(this.jdId, this.resumeId);
    });

    this.addJoinForm = this.frmbuilder.group({
      joiningId: [0],
      joiningDate: ['', Validators.required],
      offerType: ['', Validators.required],
      referenceId: ['', Validators.required],
      designation: ['', Validators.required],
      designationType: ['', Validators.required],
      amount: ['', Validators.required],
      dataAPLB: ['', Validators.required],
      payType: ['', Validators.required],
      currency: ['', Validators.required],
      resume: this.frmbuilder.group({
        resumeId: [this.resumeId, Validators.required],
      }),
      jobDescription: this.frmbuilder.group({
        id: [this.jdId, Validators.required],
      }),
    });
    this.permissionStatus();
  }
  // get join by resume id
  getJoinData(jdId, resumeId) {
    this.spinner.show();
    this.offerService.getJoinData(jdId, resumeId).subscribe(res => {
      this.spinner.hide();
      this.joinData = res;
      this.joiningId = res['joiningId'];
      this.approved_status = res['offerStatus'];
      this.salaryStructure = this.joinData.salaryStructure;
      //kamesh

    });
  }
  //getReleaseOffer
  getReleaseOffer() {
    this.spinner.show();
    this.offerService.getReleaseOffer(this.jdId, this.resumeId).subscribe(res => {
      this.spinner.hide();
      if (res) {
        this.alertService.success('Successfully!');
      }
      else {
        this.alertService.error('Error!');
      }
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.addJoinForm.controls; }
  // add join
  joinSubmit() {
    this.submitted = true;
    console.log(this.addJoinForm.value.joiningDate);
    // const newdate = formatDate(this.addJoinForm.value.joiningDate, 'yyyy-MM-dd HH:mm:ss', 'locale');
    // console.log(newdate);
    this.offerService.addJoin(this.addJoinForm.value).subscribe(res => {
      if (res) {
        $('#editoffers').modal('hide');
        this.addJoidata = res;
        this.submitted = true;
        this.getJoinData(this.jdId, this.resumeId);
        this.alertService.success('Submittted Successfully!');
      }
      else {
        this.alertService.error('Please fill all filed Value!');
        $('#editoffers').modal('hide');
      }
    });
  }
  // get jd
  getJDbyId(jdId) {
    this.jdService.getSingleJd(this.jdId).subscribe(res => {
      this.jd = res;
    });
  }

  permissionStatus() {
    this.permissionData = this.authserv.receive();
    if (this.permissionData === undefined) {
      this.authserv.setpermission2().subscribe((res) => {
        this.permissionData = res;
        this.permissionData = this.authserv.send(this.permissionData);
        if (this.permissionData) {
          console.log("erfdetail", this.permissionData);
          this.OFFERCONTROL = this.permissionData.OFFER_CONTROL;
          this.offerRelease = this.permissionData.OFFER_RELEASE;
          this.joiningDataFeedbackView = this.permissionData.JOININGDATA_FEEDBACK_VIEW;
          this.joiinindDataJdView = this.permissionData.JOININGDATA_JD_VIEW;
          this.JOININGDATAUPDATE = this.permissionData.JOININGDATA_UPDATE;
          this.JOININGDATAVIEW = this.permissionData.JOININGDATA_VIEW;

        }
      })
    } else {
      this.JOININGDATAVIEW = this.permissionData.JOININGDATA_VIEW;
      this.joiningDataFeedbackView = this.permissionData.JOININGDATA_FEEDBACK_VIEW;
      this.joiinindDataJdView = this.permissionData.JOININGDATA_JD_VIEW;
      this.JOININGDATAUPDATE = this.permissionData.JOININGDATA_UPDATE;
      this.OFFERCONTROL = this.permissionData.OFFER_CONTROL;
      this.offerRelease = this.permissionData.OFFER_RELEASE;
      console.log("erfdetail", this.permissionData);
    }
  }

  // approve offer
  approveOffer() {
    this.spinner.show();
    this.jdService.getApproveOffer(this.joiningId).subscribe(res => {
      this.spinner.hide();
      this.approved_status = res['offerStatus'];
    });
  }
  // reject joining
  rejectJoining() {
    this.spinner.show();
    console.log(this.rejectReason);
    this.jdService.getRejectOffer(this.joiningId, this.rejectReason).subscribe(res => {
      $("#reject").modal("hide");
      this.getJoinData(this.jdId, this.resumeId);
      this.spinner.hide();
    
    });
  }

}
