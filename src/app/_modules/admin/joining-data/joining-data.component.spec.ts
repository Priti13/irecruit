import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JoiningDataComponent } from './joining-data.component';

describe('JoiningDataComponent', () => {
  let component: JoiningDataComponent;
  let fixture: ComponentFixture<JoiningDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JoiningDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JoiningDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
