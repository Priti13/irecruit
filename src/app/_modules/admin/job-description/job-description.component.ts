import { Component, OnInit } from '@angular/core';
import { JdService } from 'src/app/_service/jd.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { AlertService } from 'src/app/_service/alert.service';
import{AuthenticationService} from 'src/app/_service/authentication.service';
@Component({
  selector: 'app-job-description',
  templateUrl: './job-description.component.html',
  styleUrls: ['./job-description.component.css']
})
export class JobDescriptionComponent implements OnInit {
  jds: any[] = [];
  permissionData: any;
  SeeResume: any;
  constructor(private jdService: JdService, private spinner: NgxSpinnerService, private alertService: AlertService,
    private authserv:AuthenticationService ) { }

  ngOnInit() {
    this.spinner.show();
    this.jdService.getJDDetails().subscribe((r: any[]) => {
      this.spinner.hide();
      this.jds = r
    }, err => {
      this.spinner.hide();
      this.alertService.error(err);
    })
  this.setpermission();
  }
  setpermission(){
     
    this.permissionData= this.authserv.receive();
    
    if(this.permissionData===undefined){
    this.authserv.setpermission2().subscribe((res)=>{
     this.permissionData =res;
     this.permissionData= this.authserv.send(this.permissionData);
   if( this.permissionData ){
    
   
   
    this.SeeResume=this.permissionData.RESUME_VIEW,
    
    console.log("if jobDescription",this.permissionData);
  }})
}

  else{
  
    
    this.SeeResume=this.permissionData.RESUME_VIEW,

    console.log("else jobdescription",this.permissionData);
  }
  }
}
