import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewRasReportComponent } from './view-ras-report.component';

describe('ViewRasReportComponent', () => {
  let component: ViewRasReportComponent;
  let fixture: ComponentFixture<ViewRasReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewRasReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewRasReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
