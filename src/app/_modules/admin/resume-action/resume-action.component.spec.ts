import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResumeActionComponent } from './resume-action.component';

describe('ResumeActionComponent', () => {
  let component: ResumeActionComponent;
  let fixture: ComponentFixture<ResumeActionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResumeActionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResumeActionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
