import { Component, OnInit } from '@angular/core';
import { JdService } from 'src/app/_service/jd.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { AlertService } from 'src/app/_service/alert.service';
import {AuthenticationService} from 'src/app/_service/authentication.service';


@Component({
  selector: 'app-resume-action',
  templateUrl: './resume-action.component.html',
  styleUrls: ['./resume-action.component.css']
})
export class ResumeActionComponent implements OnInit {
  jds: any[] = [];
  jobViewList:any;
  permissionData:any;
  constructor(private jdService: JdService, private spinner: NgxSpinnerService, private alertService: AlertService
    , private authserv:AuthenticationService ) { }

  ngOnInit() {
    this.setpermission();
    this.spinner.show();
    this.jdService.getJDDetails().subscribe((r: any[]) => {
      this.spinner.hide();
      this.jds = r
    }, err => {
      this.spinner.hide();
      this.alertService.error(err);
    })
    
  }

  setpermission(){
    this.spinner.show();
    this.permissionData= this.authserv.receive();
    if(this.permissionData===undefined){
      this.authserv.setpermission2().subscribe((res)=>{
     this.permissionData =res;
     this.permissionData= this.authserv.send(this.permissionData);
        if(this.permissionData){
          this.spinner.hide();
     // console.log("sidebar",this.permissionData);
      this.jobViewList=this.permissionData['JOB_VIEW'],
    
      console.log(" if resumeAction",this.permissionData);
    }})
 
    }
    else{
    
      this.jobViewList=this.permissionData['JOB_VIEW'],
      this.spinner.hide();
      console.log("else resumeAction",this.permissionData);
    }
    /*return this.authserv.permission2().subscribe(res=>{
      this.jobViewList=res.JOB_VIEW;
      console.log(this.jobViewList)
    })*/
  }
}
