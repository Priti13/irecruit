import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UserService } from 'src/app/_service/user.service';
import { AlertService } from 'src/app/_service/alert.service';
import { AuthenticationService } from 'src/app/_service/authentication.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {
  passwordForm: { oldPassword: string; newPassword: string; confirmPassword: string; };
  misMatch: boolean = false;
  constructor(
    private userService: UserService, private alertService: AlertService,
    private authService: AuthenticationService
  ) { }
  ngOnInit() {
    this.misMatch = false;
    this.passwordForm = { oldPassword: '', newPassword: '', confirmPassword: '' };
  }
  setNewPassword(f: NgForm) {
    if (f.value.newPassword !== f.value.confirmPassword) {
      this.misMatch = true;
      return;
    }
    // console.log(f.value, this.passwordForm)
    this.userService.changePassword(f.value.oldPassword, f.value.newPassword).subscribe(
      res => {
        if (res['status']) {
          f.resetForm();
          /* Logout if Password Change Successfully */
          this.authService.removeCurrentUser();

          this.alertService.success("Pease Login Again & " + res['message']);
        } else
          this.alertService.error(res['message']);
      }, err => {
        this.alertService.error(err);
      }
    )

  }
}
