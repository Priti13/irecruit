import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertService } from 'src/app/_service/alert.service';
import { AssignRecruiterService } from 'src/app/_service/assign-recruiter.service';
declare var $: any;

@Component({
  selector: 'app-assign-recruiter-detail',
  templateUrl: './assign-recruiter-detail.component.html',
  styleUrls: ['./assign-recruiter-detail.component.css']
})
export class AssignRecruiterDetailComponent implements OnInit {
  id: string;
  jobData: any;
  recruiterDetails: any[];
  selectCheckboxValue = false;
  user = [];
  constructor(private assignService: AssignRecruiterService,
    private route: ActivatedRoute,
    private alertService: AlertService, ) { }

  ngOnInit() {
    this.jobData = {};
    this.recruiterDetails = [];
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
      this.getJobDescriptionByID(this.id);
    });
    this.assignedRecruiterData();
  }
  getJobDescriptionByID(id) {
    this.assignService.jobDesById(id).subscribe(res => {
      this.jobData = res;
      this.assignService.assignedRecruitersByJd(id).subscribe((res: any[]) => {
        this.jobData['assignedRecruiter'] = res.map(r => r['user']);
      }, error => {
        this.alertService.error(error);
      });
    }, error => {
      this.alertService.error(error);
    });
  }
  assignedRecruiterData() {
    this.assignService.getassignedRecruiter().subscribe(res => {
      this.recruiterDetails = res;
    }, error => {
      this.alertService.error(error);
    });
  }
  selectedUser(event, selectedValue) {
    this.selectCheckboxValue = event.target.checked;
    if (this.selectCheckboxValue) {
      this.user = this.user.concat([selectedValue]);
    } else {
      this.user = this.user.filter(x => x.sapId !== selectedValue.sapId);
    }
    console.log(selectedValue, this.selectCheckboxValue, this.user);
  }
  tempRecruiterArr: any[];
  assignRecruiter() {
    const newObj =
    {
      "jobDescription": {
        "id": this.id
      },
      "user": this.user
    };
    this.jobData.assignedRecruiter = this.user;
    this.alertService.success('Submittted Successfully!');
    $('#addAssign').modal('hide');
     this.assignService.assignRecruiter(newObj).subscribe(res => {
      if (res) {
        this.jobData.assignedRecruiter = res['user'];
        $('#addAssign').modal('hide');
        this.user = [];
        this.alertService.success('Submittted Successfully!');
        this.assignedRecruiterData();
      } else {
        this.alertService.error(res['msg']);
      }
    }), error => {
      this.alertService.error(error);
    }; 
  }
  acsetting() {
    //  strat hua h function
    //   hourDatacount.forEach(function (a) {

    //     for (let i = 0; i < hourDatacount.length; i++) {
    //       for (let j = 0; j < brandArray.length; j++) {
    //         if (hourDatacount[i].brand !== brandArray[j]) {
    //           this[a.brand] = { brand: a.brand, count: 0 };
    //           uniqueArr.push(this[a.brand]);
    //         }
    //         this[a.brand].count += a.count;
    //       }
    //     }
    //   }, Object.create(null));
  }
}
