import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignRecruiterDetailComponent } from './assign-recruiter-detail.component';

describe('AssignRecruiterDetailComponent', () => {
  let component: AssignRecruiterDetailComponent;
  let fixture: ComponentFixture<AssignRecruiterDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignRecruiterDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignRecruiterDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
