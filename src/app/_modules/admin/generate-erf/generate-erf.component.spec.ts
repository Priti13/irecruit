import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateErfComponent } from './generate-erf.component';

describe('GenerateErfComponent', () => {
  let component: GenerateErfComponent;
  let fixture: ComponentFixture<GenerateErfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerateErfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateErfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
