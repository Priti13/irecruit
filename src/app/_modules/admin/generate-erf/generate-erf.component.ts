import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AlertService } from 'src/app/_service/alert.service';
import { SharedService } from 'src/app/_service/shared.service';
import { ERFService } from 'src/app/_service/erf.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { IDropdownSettings } from 'ng-multiselect-dropdown';


@Component({
  selector: 'app-generate-erf',
  templateUrl: './generate-erf.component.html',
  styleUrls: ['./generate-erf.component.css']
})
export class GenerateErfComponent implements OnInit {
  salesSPOCdropdownList = [];
  buHeaddropdownList = [];
  dropdownSettings: IDropdownSettings = {};

  data: any;
  generateForm: GenerateErForm;
  submitted = false;
  countryList: any;
  list: any;
  projectsList: any;
  officeLocationsList: any;
  resourceAllocationsList: any;
  bussinessUnit: string;
  stateList: any;
  sourceAddresses: { "country": any; "state": any; "erf": any; "zipCode": string; "addressLineOne": string; }[];
  workAddresses: { "country": any; "state": any; "erf": any; "zipCode": string; "addressLineOne": string; }[];
  stateListWRK: any;

  constructor(
    private frmBuilder: FormBuilder,
    private sharedService: SharedService,
    private alertService: AlertService,
    private erfService: ERFService,
    private spinner: NgxSpinnerService
  ) { }
  ngOnInit() {
    this.bussinessUnit = JSON.parse(localStorage.getItem('currentUser'));
    this.getAllList();
    this.initiateSourceWorkAddress();

  }
  initiateSourceWorkAddress() {
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'sapId',
      textField: 'username',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };
    this.generateForm = {
      buhead: [],
      projectName: '',
      projectDuration: '',
      salesSPOC: [],
      primaryClient: '',
      endClient: '',
      totalPosition: null,
      maxBudget: null,
      currency: 'INR',
    }
    this.generateForm.buhead = [];
    this.generateForm.salesSPOC = [];

    this.erfService.getSaleSpoc().subscribe(
      (res: any[]) => this.salesSPOCdropdownList = res
    )
    this.erfService.getBuHead().subscribe(
      (res: any[]) => this.buHeaddropdownList = res
    )
    this.sourceAddresses = [{
      "country": 'Select Country',
      "state": 'Select State',
      "erf": null,
      "zipCode": '',
      "addressLineOne": ''
    }];
    this.workAddresses = [{
      "country": 'Select Country',
      "state": 'Select State',
      "erf": null,
      "zipCode": '',
      "addressLineOne": ''
    }];
  }
  onItemSelectBuHead(item: any) {
    this.generateForm.buhead.push(item.sapId);
  }
  onSelectAllBuHead(item: any) {
    this.generateForm.buhead = item.map(r => r.sapId);
  }
  onItemSelectSalesSPOC(item: any) {
    this.generateForm.salesSPOC.push(item.sapId);
  }
  onSelectAllSalesSPOC(item: any) {
    this.generateForm.salesSPOC = item.map(r => r.sapId);
  }
  addWorkAddress() {
    const newValue = {
      "country": {},
      "state": '',
      "erf": null,
      "zipCode": '',
      "addressLineOne": ''
    }
    this.workAddresses.push(newValue);
  }
  deleteWorkAddress(newObj) {
    this.workAddresses = this.workAddresses.filter(obj => obj !== newObj);
  }
  addSourceAddress() {
    const newValue = {
      "country": '',
      "state": '',
      "erf": null,
      "zipCode": '',
      "addressLineOne": ''
    }
    this.sourceAddresses.push(newValue);
  }
  deleteSourceAddress(newObj) {
    this.sourceAddresses = this.sourceAddresses.filter(obj => obj !== newObj);
  }
  // get all state by country id for source
  getStateByCountry(countryId) {
    this.sharedService.getStateList(countryId).subscribe(res => {
      this.stateList = res;
    });
  }
  onCountryChangeSRC(selectedCountry) {
    this.getStateByCountry(selectedCountry.value.countryId);
  }
  // get all state by country id for wrok
  getStateByCountryWRK(countryId) {
    this.sharedService.getStateList(countryId).subscribe(res => {
      this.stateListWRK = res;
    });
  }
  onCountryChangeWRK(selectedCountry) {
    this.getStateByCountryWRK(selectedCountry.value.countryId);
  }
  getAllList() {
    this.spinner.show();
    this.erfService.getAllDetailsForAdd().subscribe(res => {
      this.spinner.hide();
      this.list = res;
      this.projectsList = res['projects'];
      this.officeLocationsList = res['officeLocations'];
      this.countryList = res['countries'];
      this.resourceAllocationsList = res['resourceAllocations'];
    }, error => {
      this.spinner.hide();
      this.alertService.error(error);
    });
  }
  // get f() { return this.generateErForm.controls }
  // save form data
  generateErf() {
    // if (this.generateForm['billable'] === 'true') {
    //   this.generateForm['billable'] = true;
    // } else {
    //   this.generateForm['billable'] = false;
    // }
    this.generateForm['erfId'] = 0;
    this.generateForm['sourceAddresses'] = this.sourceAddresses;
    this.generateForm['workAddresses'] = this.workAddresses;
    this.spinner.show();
    this.erfService.saveErf(this.generateForm).subscribe(res => {
      if (res) {
        this.spinner.hide();
        this.data = res;
        this.alertService.success('Submittted Successfully!');
        // this.initiateSourceWorkAddress();
        location.reload();
      } else {
        this.spinner.hide();
      }
    }, error => { // 'Please fill all Correct Value!'
      this.spinner.hide();
      this.alertService.error(error);
    });
  }
  // get object by id of object
  getObjectById(id, arry, name) {
    for (let i = 0; i < arry.length; i++) {
      if (name === 'project') {
        if (parseInt(id) === arry[i].projectId) {
          return arry[i];
        }
      }
      if (name === 'workLocation') {
        if (parseInt(id) === arry[i].id) {
          return arry[i];
        }
      }
      if (name === 'resourceLocation') {
        if (parseInt(id) === arry[i].resouceAllocationId) {
          return arry[i];
        }
      }
    }
  }
  getObjectByIdArray(arry) {
    //  for(let i = 0; i<arry.length; i++){
    //    for(let k = 0; k< this.countryList.length; k++){
    //     if(arry[i].country == this.countryList[k].id){
    //       return arry[i].country = this.countryList[k];
    //     }
    //    }
    //    for(let k = 0; k< this.stateList.length; k++){
    //     if(arry[i].country == this.countryList[k].id){
    //       return arry[i].country = this.countryList[k];
    //     }
    //    }


    //  }
  }
}

interface GenerateErForm {
  buhead: string[];
  projectName: String;
  projectDuration: String;
  salesSPOC: string[];
  primaryClient: String;
  endClient: String;
  totalPosition: number;
  maxBudget: number;
  currency: string;
}