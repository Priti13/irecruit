import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from 'src/app/_service/authentication.service';

@Component({
  selector: 'app-admin-setting',
  templateUrl: './admin-setting.component.html',
  styleUrls: ['./admin-setting.component.css']
})
export class AdminSettingComponent implements OnInit {
  empPer:any;
  permissionData:any;

  constructor(private authserv:AuthenticationService) { }

  ngOnInit() {
    this.setpermissionn();
  }

  setpermissionn(){
    this.permissionData= this.authserv.receive();
    if( this.permissionData===undefined){
      this.authserv.setpermission2().subscribe(res=>{
        this.permissionData=res;
        this.authserv.send(this.permissionData);
       // console.log("sideapi",this.permissionData);
        
    
    this.empPer=this.permissionData['EMPLOYEE_PERMISSION'],
    console.log("if adminseeting",this.permissionData);
  
  })}
  else{
  
    this.empPer=this.permissionData['EMPLOYEE_PERMISSION'],
    
    console.log("else adminseeting",this.permissionData);
  }
    /*return this.authserv.permission2().subscribe(res=>{
      this.empPer=res.EMPLOYEE_PERMISSION;
      console.log(this.empPer)
    })*/
  }
}
