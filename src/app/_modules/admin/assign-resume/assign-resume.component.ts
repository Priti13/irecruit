import { Component, OnInit } from '@angular/core';
import { JdService } from 'src/app/_service/jd.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { AlertService } from 'src/app/_service/alert.service';
import { AuthenticationService } from 'src/app/_service/authentication.service';

@Component({
  selector: 'app-assign-resume',
  templateUrl: './assign-resume.component.html',
  styleUrls: ['./assign-resume.component.css']
})
export class AssignResumeComponent implements OnInit {
  jobDes: any[];
  candidateUpload:any;
  permissionData:any;

  constructor(private jdService: JdService, private spinner: NgxSpinnerService, private alertService: AlertService, private mypermissionService: AuthenticationService) { }

  ngOnInit() {
    this.spinner.show();
    this.jdService.getJDDetails().subscribe((r: any[]) => {
      this.spinner.hide();
      this.jobDes = r
    }, err => {
      this.spinner.hide();
      this.alertService.error(err);
    })
    this.permissionstatus();
  }
  permissionstatus(){
    this.permissionData= this.mypermissionService.receive();
    if(this.permissionData===undefined){
      this.mypermissionService.setpermission2().subscribe((res)=>{
     this.permissionData =res;
     this.permissionData= this.mypermissionService.send( this.permissionData);
        if(this.permissionData){
    this.candidateUpload=this.permissionData['CANDIDATE_UPLOAD'],
    console.log("if assignResume",this.permissionData);
  
  }})}
  else{
  
    this.candidateUpload=this.permissionData['CANDIDATE_UPLOAD'],
    
    console.log(" else assignResume",this.permissionData);
  }
    /*return this.mypermissionService.permission2().subscribe(res=>{
    this.candidateUpload=res.CANDIDATE_UPLOAD;
    console.log(this.candidateUpload)})
  */
    }
}
