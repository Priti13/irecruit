import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignResumeComponent } from './assign-resume.component';

describe('AssignResumeComponent', () => {
  let component: AssignResumeComponent;
  let fixture: ComponentFixture<AssignResumeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignResumeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignResumeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
