import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertService } from 'src/app/_service/alert.service';
import { SharedService } from 'src/app/_service/shared.service';
import { UploadService } from 'src/app/_service/upload.service';


@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {

  // upload forms
  uploadResume: FormGroup;
  submitted = false;

  fileToUpload: File = null;
  uploadSuccess: boolean;
  countryList: any;
  stateList: any;
  fileExtensionMessage: any;
  permissionData:any;

  constructor(
    private uploadService: UploadService,
    private frmBuilder: FormBuilder,
    private alertService: AlertService,
    private sharedService: SharedService
  ) { 
   // this.permissionstatus();
  }

  ngOnInit() {
    this.uploadResume = this.frmBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      phone: ['', Validators.required],
      domain: ['', Validators.required],
      countryId: ['', Validators.required],
      address: ['', Validators.required],
      city: ['', Validators.required],
      stateId: ['', Validators.required],
      zip: ['', Validators.required],
      file: ['', Validators],
    });
    this.getCountry();
  
  }
  // convenience getter for easy access to form fields
  get f() { return this.uploadResume.controls; }

  // get all country list
  getCountry() {
    this.sharedService.getCountryList().subscribe(res => {
      this.countryList = res;
    });
  }
  // get all state by country id
  getStateByCountry(countryId) {
    this.sharedService.getStateList(countryId).subscribe(res => {
      console.log(res);
      this.stateList = res;
    });
  }
  onCountryChange(value) {
    this.getStateByCountry(value);
  }
  handleFileInput(files: FileList) {
    this.uploadResume.value.file = files[0];
    const fileList: FileList = files;
    // File Formate: .docx, .doc, .Pdf, .odt and .pages

    if (fileList && fileList[0] && fileList.length > -1) {
      if (
        fileList[0].type === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document' ||
        fileList[0].type === 'application/msword' ||
        fileList[0].type === 'application/pdf' ||
        fileList[0].type === 'text/plain' ||
        fileList[0].type === 'application/vnd.oasis.opendocument.text' ||
        fileList[0].type === 'application/vnd.apple.pages'
      ) {
        // this.uploadResume.value.file = fileList[0].name;
        // console.log(fileList[0].name)
        // console.log(fileList[0])
        // if (
        //   ((fileList[0].type).indexOf('application/pdf') > -1) ||
        //   ((fileList[0].type).indexOf('text/plain') > -1) ||
        //   ((fileList[0].type).indexOf('application/msword') > -1) 
        // ) {
          this.uploadResume.value.file = fileList[0];
        // }
      }
    } else {
      this.alertService.error('Upload PDF, doc, docx, txt Format File Only!! ');
    }
  }
  submitResume() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.uploadResume.invalid) {
      return;
    } else {
      this.uploadService.uploadResume(this.uploadResume.value).subscribe(res => {
        if (res) {
          this.uploadResume.reset();
          this.submitted = false;
          // Object.keys(this.uploadResume.controls).forEach(key => {
          //   this.uploadResume.get(key).setErrors(null);
          // });
          this.alertService.success('Uploaded successfully.');
        }
      }, err => {
        this.alertService.error(err);
      });
    }
  }
  //dynamic permission shantam
  
}
