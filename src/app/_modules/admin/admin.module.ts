import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AdminRoutingModule } from './admin-routing.module';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './header/header.component';
import { SideBarComponent } from './side-bar/side-bar.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FooterComponent } from './footer/footer.component';
import { ViewErfComponent } from './view-erf/view-erf.component';
import { GenerateErfComponent } from './generate-erf/generate-erf.component';
import { ErfdetailComponent } from './erfdetail/erfdetail.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AlertComponent } from './alert/alert.component';
import { UploadComponent } from './upload/upload.component';
import { QuillModule } from 'ngx-quill';
import { TagInputModule } from 'ngx-chips';
import { AssignRecruiterComponent } from './assign-recruiter/assign-recruiter.component';
import { AssignRecruiterDetailComponent } from './assign-recruiter-detail/assign-recruiter-detail.component';
import { AssignResumeComponent } from './assign-resume/assign-resume.component';
import { JobDescriptionComponent } from './job-description/job-description.component';
import { OffersComponent } from './offers/offers.component';
import { ViewResumeComponent } from './view-resume/view-resume.component';
import { SetPanelComponent } from './set-panel/set-panel.component';
import { SetCalendarComponent } from './set-calendar/set-calendar.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { JoiningDataComponent } from './joining-data/joining-data.component';
// import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerModule } from "ngx-spinner";
import { ResumeDetailsComponent } from './resume-details/resume-details.component';
import { ResumeActionComponent } from './resume-action/resume-action.component';
import { AssignedResumeComponent } from './assigned-resume/assigned-resume.component';
import { DatepickerModule, BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { FeedbackViewComponent } from './feedback-view/feedback-view.component';
import { ProfileComponent } from './profile/profile.component';
import { SettingComponent } from './setting/setting.component';
import { CreatePermissionComponent } from './create-permission/create-permission.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { SetpermissionComponent } from './setpermission/setpermission.component';
import { ReportComponent } from './report/report.component';
import { SetFeedbackComponent } from './set-feedback/set-feedback.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { AdminSettingComponent } from './admin-setting/admin-setting.component';
import { EmployeeDetailComponent } from './employee-detail/employee-detail.component';
import { SetemployeeDetailComponent } from './setemployee-detail/setemployee-detail.component';
import { RasDetailComponent } from './ras-detail/ras-detail.component';
import { RasDetailViewComponent } from './ras-detail-view/ras-detail-view.component';
import { RasReportComponent } from './ras-report/ras-report.component';
import { RasReportDetailComponent } from './ras-report-detail/ras-report-detail.component';
import { ViewRasReportComponent } from './view-ras-report/view-ras-report.component';
import { ExternalfeedbackComponent } from './externalfeedback/externalfeedback.component';
import { ThemeSettingComponent } from './theme-setting/theme-setting.component';


@NgModule({
  declarations: [
    LoginComponent,
    HeaderComponent,
    SideBarComponent,
    DashboardComponent,
    FooterComponent,
    ViewErfComponent,
    GenerateErfComponent,
    ErfdetailComponent,
    AlertComponent,
    UploadComponent,
    AssignRecruiterComponent,
    AssignRecruiterDetailComponent,
    AssignResumeComponent,
    JobDescriptionComponent,
    OffersComponent,
    ViewResumeComponent,
    SetPanelComponent,
    SetCalendarComponent,
    FeedbackComponent,
    FeedbackViewComponent,
    JoiningDataComponent,
    ResumeDetailsComponent,
    ResumeActionComponent,
    AssignedResumeComponent,
    ProfileComponent,
    SettingComponent,
    CreatePermissionComponent,
    SetpermissionComponent,
    ForgotPasswordComponent,
    ChangePasswordComponent,
    ReportComponent,
    SetFeedbackComponent,
    SetFeedbackComponent,
    AdminSettingComponent,
    EmployeeDetailComponent,
    SetemployeeDetailComponent,
    RasDetailComponent,
    RasDetailViewComponent,
    RasReportComponent,
    RasReportDetailComponent,
    ViewRasReportComponent,
    ExternalfeedbackComponent,
    ThemeSettingComponent,


  ],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    AdminRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    TagInputModule,
    NgxSpinnerModule,
    QuillModule.forRoot({
      modules: {
        syntax: true,
        toolbar: []
      }
    }),
    BsDatepickerModule.forRoot(),
    DatepickerModule.forRoot(),
    NgMultiSelectDropDownModule.forRoot(),
  ],
  exports: [
    LoginComponent,
    HeaderComponent,
    SideBarComponent,
    DashboardComponent,
    FooterComponent,
    ViewErfComponent,
    GenerateErfComponent,
    ErfdetailComponent,
    AlertComponent,
    UploadComponent,
    AssignRecruiterComponent,
    AssignRecruiterDetailComponent,
    AssignResumeComponent,
    JobDescriptionComponent,
    OffersComponent,
    ViewResumeComponent,
    SetPanelComponent,
    SetCalendarComponent,
    FeedbackComponent,
    FeedbackViewComponent,
    JoiningDataComponent,
    ResumeDetailsComponent,
    ResumeActionComponent,
    AssignedResumeComponent,
    ProfileComponent,
    SettingComponent,
    CreatePermissionComponent,
    SetpermissionComponent,
    ForgotPasswordComponent,
    ChangePasswordComponent,
    ReportComponent,
    SetFeedbackComponent,
    AdminSettingComponent,
    EmployeeDetailComponent,
    SetemployeeDetailComponent,
    RasDetailComponent,
    RasDetailViewComponent,
    ViewRasReportComponent,
    ThemeSettingComponent
  ],
})
export class AdminModule { }
