import { Component, OnInit } from '@angular/core';
import { AssignRecruiterService} from 'src/app/_service/assign-recruiter.service';
@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.css']
})
export class EmployeeDetailComponent implements OnInit {

  constructor(private assignRecruiServ:AssignRecruiterService) { }
empdetail:any;
employeelist:any;
jdId:any;
resumeId:any;
  ngOnInit() {
    this.assignRecruiServ.createpermission().subscribe((res)=>{
      this.empdetail=res
    
    })
    this.getEmpList();
  }
getEmpList(){
  return this.assignRecruiServ.getOfferCandidatelist().subscribe((res)=>{
    this.employeelist=res;
    console.log("list"+JSON.stringify(res));
  });
}
postEmpDetail(employeeDetails){
  this.jdId=employeeDetails.jdId;
  this.resumeId=employeeDetails.resumeId;
console.log(employeeDetails);
   this.assignRecruiServ.postEmpDetail(this.jdId,this.resumeId).subscribe((res)=>console.log(res));
}
}
