import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RasDetailComponent } from './ras-detail.component';

describe('RasDetailComponent', () => {
  let component: RasDetailComponent;
  let fixture: ComponentFixture<RasDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RasDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RasDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
