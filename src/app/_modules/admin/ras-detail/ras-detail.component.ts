import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { AlertComponent } from '../alert/alert.component';
import { GenerateReport } from 'src/app/_service/generate-report.services';
import { AlertService } from 'src/app/_service/alert.service';


@Component({
  selector: 'app-ras-detail',
  templateUrl: './ras-detail.component.html',
  styleUrls: ['./ras-detail.component.css']
})
export class RasDetailComponent implements OnInit {
  reportData: [];

  selectedLevel;
  public size: number;
  public square: number;
  public dtModelStart: any;
  public SelectStatus: any;
  public dtModelEnd: any;
  result: { filterDate: { startDate: string; endDate: string; }; erfStatus: string; };
  data: never;
  id: any;
  constructor(
    private generateReport: GenerateReport,
    private spinner: NgxSpinnerService,
    private alertService: AlertService
  ) {
  }

  ngOnInit() {

    this.ReportData();
  }

  ReportData() {
    this.spinner.show();
    this.generateReport.getReportData().subscribe(res => {
      this.spinner.hide();
      this.reportData = res;
    });

  }

  StatusList: Array<Object> = [
    { name: "OPEN" },
    { name: "CLOSE" },
    { name: "ALL" }
  ];
  selected() {
    //console.log(this.selectedLevel)
  }

  searchList() {

    if ((this.dtModelStart && this.dtModelEnd) || this.selectedLevel) {

      console.log("this.dtModelStart", this.dtModelStart);
      console.log("this.dtModelEnd", this.dtModelEnd);
      console.log("this.selectedLevel", this.selectedLevel);

      if ((Date.parse(this.dtModelEnd) <= Date.parse(this.dtModelStart))) {
        this.alertService.success('End date should be greater than Start date');
      }
      else {
        this.spinner.show();
        //!obj.x
        if (!this.selectedLevel) {
          this.result = {
            "filterDate":
            {
              "startDate": this.dtModelStart,
              "endDate": this.dtModelEnd
            },
            "erfStatus": ''
          }
        }
        else if (!this.dtModelStart && !this.dtModelEnd) {
          this.result = {
            "filterDate":
            {
              "startDate": '',
              "endDate": ''
            },
            "erfStatus": this.selectedLevel.name
          }
        }


        // this.result = {
        //   "filterDate":
        //   {
        //     "startDate": '',
        //     "endDate": ''
        //   },
        //   "erfStatus": ''
        // }

        this.generateReport.filterReport(this.result).subscribe(res => {
          this.spinner.hide();
          this.reportData = res;


        });
      }

    }
  }

  exportExcel() {
    console.log('jkfbasb');
    // if ((this.dtModelStart && this.dtModelEnd) || this.selectedLevel) {

      console.log("this.dtModelStart", this.dtModelStart);
      console.log("this.dtModelEnd", this.dtModelEnd);
      console.log("this.selectedLevel", this.selectedLevel);

      if ((Date.parse(this.dtModelEnd) <= Date.parse(this.dtModelStart))) {
        this.alertService.success('End date should be greater than Start date');
      }
      else {
        this.spinner.show();
        //!obj.x
        if (!this.selectedLevel) {
          this.result = {
            "filterDate":
            {
              "startDate": this.dtModelStart,
              "endDate": this.dtModelEnd
            },
            "erfStatus": ''
          }
        }
        else if (!this.dtModelStart && !this.dtModelEnd) {
          this.result = {
            "filterDate":
            {
              "startDate": '',
              "endDate": ''
            },
            "erfStatus": this.selectedLevel.name
          }
        }


        // this.result = {
        //   "filterDate":
        //   {
        //     "startDate": '',
        //     "endDate": ''
        //   },
        //   "erfStatus": ''
        // }

        this.generateReport.downloadReport(this.result).subscribe(res => {
          this.spinner.hide();
          window.open(res, "_blank");
          console.log("res", res);
        });
      }

    // }

  }

  indexnumber(i) {
    this.id = i;
    this.data = this.reportData[this.id];
    this.generateReport.setData(this.data);
    localStorage.setItem('id', this.id);
  }

}
