import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JdService } from 'src/app/_service/jd.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { AlertService } from 'src/app/_service/alert.service';
import { UploadService } from 'src/app/_service/upload.service';
import {AuthenticationService} from 'src/app/_service/authentication.service';

@Component({
  selector: 'app-assigned-resume',
  templateUrl: './assigned-resume.component.html',
  styleUrls: ['./assigned-resume.component.css']
})
export class AssignedResumeComponent implements OnInit {
  jdId: string;
  allAssignedResumes = [];
  //shantam
  permissionData:any;
  SeeResume:any;
  constructor(private route: ActivatedRoute, private jdService: JdService,
    private spinner: NgxSpinnerService, private alertService: AlertService,
    private authserv:AuthenticationService) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.jdId = params.get('id')
    });
    this.assignedResumes();
    this.setpermission();
  }
    // already assigned resumes
    assignedResumes(){
      this.spinner.show();
      this.jdService.getResumesAttached(this.jdId).subscribe((res: any[]) => {
        if (res) {
          this.spinner.hide();
          this.allAssignedResumes = res;
        }
      }, err => {
        this.spinner.hide();
        this.alertService.error(err);
      })
    }
    setpermission(){
     
      this.permissionData= this.authserv.receive();
      if(this.permissionData===undefined){
        this.authserv.setpermission2().subscribe((res)=>{
       this.permissionData =res;
       this.permissionData= this.authserv.send( this.permissionData);
     if( this.permissionData ){
      
     
     
      this.SeeResume=this.permissionData.RESUME_VIEW,
      
      console.log("if seeresume",this.permissionData);
    }})
  }
  
    else{
    
      
      this.SeeResume=this.permissionData.RESUME_VIEW,
 
      console.log("else seeresume",this.permissionData);
    }
     /* return this.authserv.permission2().subscribe(res=>{
        this.calenderView=res.CALENDAR_VIEW;
        this.feedBackView=res.FEEDBACK_VIEW;
        console.log(this.calenderView,this.feedBackView);
      });*/
    }
  }

