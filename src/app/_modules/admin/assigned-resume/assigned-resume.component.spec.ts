import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignedResumeComponent } from './assigned-resume.component';

describe('AssignedResumeComponent', () => {
  let component: AssignedResumeComponent;
  let fixture: ComponentFixture<AssignedResumeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignedResumeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignedResumeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
