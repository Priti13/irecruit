import { Component, OnInit } from '@angular/core';
import { OffersService } from 'src/app/_service/offers.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-offers',
  templateUrl: './offers.component.html',
  styleUrls: ['./offers.component.css']
})
export class OffersComponent implements OnInit {
  offersData: any = [];

  constructor(private offerService: OffersService, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    this.spinner.show();
    this.getOffer()
  }
  getOffer() {
    this.offerService.getOffers().subscribe(res => {
      this.spinner.hide();
      this.offersData = res;
      console.log('Offer Data', this.offersData);
    });
  }
}
