import { Component, OnInit } from '@angular/core';
import { JdService } from 'src/app/_service/jd.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'src/app/_service/alert.service';
import { Location } from '@angular/common';
import { OffersService } from 'src/app/_service/offers.service';
import { ERFService } from 'src/app/_service/erf.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { UploadService } from 'src/app/_service/upload.service';
import { DomSanitizer } from '@angular/platform-browser';
import { AuthenticationService } from 'src/app/_service/authentication.service';
declare var $: any;

@Component({
  selector: 'app-set-feedback',
  templateUrl: './set-feedback.component.html',
  styleUrls: ['./set-feedback.component.css']
})
export class SetFeedbackComponent implements OnInit {
  jdId: string;
  resumeId: string;
  setPanelData: any[];
  round = 0;
  jdDetails: any;
  jdResume: any;
  resumeLink: any;
  getfeedback: any[];
  skill: any[];
  interviewDetail: any[];
  otherRole: any[];
  //shantam
  feedbackPanel:any;
  feedbackAdd:any;
  feedbackViewJd:any;
  feedbackViewResume:any;
  permissionData:any;

  constructor(
    private jdService: JdService, private route: ActivatedRoute, private offersService: OffersService,
    private alertService: AlertService, private router: Router, private spinner: NgxSpinnerService,
    private uploadService: UploadService, public sanitizer: DomSanitizer, private location: Location,
    private mypermissionService: AuthenticationService
  ) { }
  ngOnInit() {
    this.setPanelData = [];
    this.jdDetails = {};
    this.jdResume = {};
    this.route.paramMap.subscribe(params => {
      this.resumeId = params.get('resumeId');
      this.jdId = params.get('jdId');
      this.getSetPanelData(this.jdId, this.resumeId);
      this.getJD_Resumes();
    });
    this.setPermission();
  }
  getSetPanelData(id, resumeid) {
    this.jdService.getsetPanel(id, resumeid).subscribe(res => {
      if (res) {
        this.setPanelData = res;
        const len = this.setPanelData.length - 1;
        this.round = this.setPanelData[len].round;
      }
    }, err => {
      this.alertService.error(err);
    });
  }
  openSetFeedback(interviewObj) {
    if (interviewObj.feedbackView) {
      console.log("interviewObj.feedbackView", interviewObj);
      this.getFeedbackData(interviewObj.id);
      $("#viewFeedack").modal("show"); // toggle, show, hide

    } else
      this.router.navigate(['/feedback', this.jdId, this.resumeId, interviewObj.id])
  }
  getFeedbackData(interviewId) {
    this.offersService.getInterviewFeedback(interviewId).subscribe(
      res => {
        this.getfeedback = res;
        this.skill = res.skill;
        this.interviewDetail = res.interviewDetail.internalPanel;
        this.otherRole = res.otherRole;
        console.log("this.interviewDetail", this.interviewDetail);
      }
    )
  }
  getJD_Resumes() {
    this.jdService.getJDResumeDetail(this.jdId, this.resumeId).subscribe((res: any[]) => {
      this.jdDetails = res['jobDescription']
      this.jdResume = res['resumeData']
    }, error => {
      this.alertService.error(error);
    })
  }
  showResume() {
    this.spinner.show();
    this.uploadService.getResumeDetailsFile(this.jdResume['resumeId']).subscribe(res => {
      this.spinner.hide();

      let extensionType: string = '';
      if (this.jdResume['fileType'] === "pdf") {
        extensionType = "application/pdf";
      } else if (this.jdResume['fileType'] === "doc") {
        extensionType = "application/msword";
        extensionType = "application/pdf";
      } else if (this.jdResume['fileType'] === "docx") {
        extensionType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
      } else if (this.jdResume['fileType'] === "txt") {
        extensionType = "text/plain";
      } else if (this.jdResume['fileType'] === "odt") {
        extensionType = "application/vnd.oasis.opendocument.text";
      } else if (this.jdResume['fileType'] === "pages") {
        extensionType = "application/vnd.apple.pages";
      } else {
        extensionType = "application/octet-stream";
      }

      const blob: Blob = new Blob([res], { type: extensionType });
      const fileURL: string = window.URL.createObjectURL(blob);

      if (this.jdResume['fileType'] === "docx") {
        window.open(fileURL)
      } else {  // pdf, txt
        this.resumeLink = fileURL;
      }
    }, err => {
      this.spinner.hide();
      this.alertService.error(err);
    })
  }
  goBack() {
    this.location.back();
  }
  setPermission(){
    this.permissionData= this.mypermissionService.receive();
    if(this.permissionData===undefined){
      this.mypermissionService.setpermission2().subscribe((res)=>{
     this.permissionData =res,
     this.permissionData= this.mypermissionService.send(this.permissionData);
        console.log("sideapi",this.permissionData);
        if(this.permissionData){
    console.log("setfeedback",this.permissionData);
    this.feedbackPanel=this.permissionData['FEEDBACK_VIEW_PANEL'],
    this.feedbackAdd=this.permissionData.FEEDBACK_ADD,
    this.feedbackViewJd=this.permissionData.FEEDBACK_VIEW_JD,
    console.log("setfeedback",this.permissionData);
 }})}
  else{
  
    this.feedbackPanel=this.permissionData['FEEDBACK_VIEW_PANEL'],
    this.feedbackAdd=this.permissionData.FEEDBACK_ADD,
    this.feedbackViewJd=this.permissionData.FEEDBACK_VIEW_JD,

    console.log("setfeedback",this.permissionData);
  }
 
    /*return this.mypermissionService.permission2().subscribe(res=>{this.feedbackPanel= res.FEEDBACK_VIEW_PANEL
      this.feedbackAdd=res.FEEDBACK_ADD;

this.feedbackViewJd=res.FEEDBACK_VIEW_JD;

this.feedbackViewResume=res.FEEDBACK_VIEW_RESUME;
console.log(this.feedbackViewJd,this.feedbackViewResume)
    })*/
  }
}
