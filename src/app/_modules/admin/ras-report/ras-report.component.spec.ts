import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RasReportComponent } from './ras-report.component';

describe('RasReportComponent', () => {
  let component: RasReportComponent;
  let fixture: ComponentFixture<RasReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RasReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RasReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
