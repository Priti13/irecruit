import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignRecruiterComponent } from './assign-recruiter.component';

describe('AssignRecruiterComponent', () => {
  let component: AssignRecruiterComponent;
  let fixture: ComponentFixture<AssignRecruiterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignRecruiterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignRecruiterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
