import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AlertService } from 'src/app/_service/alert.service';
import { AssignRecruiterService } from 'src/app/_service/assign-recruiter.service';
import {AuthenticationService} from 'src/app/_service/authentication.service';

@Component({
  selector: 'app-assign-recruiter',
  templateUrl: './assign-recruiter.component.html',
  styleUrls: ['./assign-recruiter.component.css']
})
export class AssignRecruiterComponent implements OnInit {

  jobDesData: any;
  //shantam
  permissionData:any;
  recruiterAssign:any;
  constructor(private assignService: AssignRecruiterService,
    private route: ActivatedRoute,
    private alertService: AlertService,
    private authserv:AuthenticationService ) { }

  ngOnInit() {
    this.jobDescriptionData();
    this.setpermission();
  }
  
    // job description start here
    jobDescriptionData(){
      this.assignService.jobDescriptionDetails().subscribe(res => {
        this.jobDesData = res;
        }), error => {
          this.alertService.error(error);
        };
     
    }
    setpermission(){
      
      this.permissionData= this.authserv.receive();
      if(this.permissionData===undefined){
        this.authserv.setpermission2().subscribe((res)=>{
       this.permissionData =res;
       this.permissionData= this.authserv.send(this.permissionData);
          if(this.permissionData){
            
       
        this.recruiterAssign=this.permissionData['RECRUITER_VIEW'],
      
        console.log(" if recruiterview",this.permissionData);
      }})
  
      }
      else{
      
        this.recruiterAssign=this.permissionData['RECRUITER_VIEW'],
        
        console.log("else recruiterview",this.permissionData);
      }
//RECRUITER_ASSIGN
}
}