import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JdService } from 'src/app/_service/jd.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { AlertService } from 'src/app/_service/alert.service';
import { UploadService } from 'src/app/_service/upload.service';
import { DomSanitizer } from '@angular/platform-browser';
import {AuthenticationService} from 'src/app/_service/authentication.service';
declare var $: any;

@Component({
  selector: 'app-resume-details',
  templateUrl: './resume-details.component.html',
  styleUrls: ['./resume-details.component.css']
})
export class ResumeDetailsComponent implements OnInit {
  jdId: string;
  allAssignedResumes: any[] = [];
  resumesData: any[] = [];

  // search by dropdown
  searchBy: any;
  searchByValue: any;
  selectBy = 'Search By';

  // TODO: assign resume
  resumes = [];
  // TODO: for show in iframe
  resumeLink: any;
  //Shantam
  permissionData:any;
  seeResume:any;

  constructor(private route: ActivatedRoute, private jdService: JdService,
    private spinner: NgxSpinnerService, private alertService: AlertService, private uploadService: UploadService,
    public sanitizer: DomSanitizer,private authserv:AuthenticationService ) { }
  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.jdId = params.get('id')
    });
    this.assignedResumes();
    this.setpermission();
  }
  // already assigned resumes
  assignedResumes() {
    this.spinner.show();
    this.jdService.getResumesAttached(this.jdId).subscribe((res: any[]) => {
      if (res) {
        this.spinner.hide();
        this.allAssignedResumes = res;
      }
    }, err => {
      this.spinner.hide();
      this.alertService.error(err);
    })
  }
  // search in table
  searchByDropdown(selectedValue) {
    this.searchByValue = selectedValue;
  }
  searchTable(searchValue) {
    console.log(searchValue.value);
    if (this.searchByValue === 'email' && searchValue.value) {
      this.searchByEmail(searchValue.value)
    } else if (this.searchByValue === 'firstName' && searchValue.value) {
      this.searchByFirstName(searchValue.value)
    } else if (this.searchByValue === 'lastName' && searchValue.value) {
      this.searchByLastName(searchValue.value)
    } else if (this.searchByValue === 'domain' && searchValue.value) {
      this.searchByDomain(searchValue.value)
    } else {
      this.alertService.error('Select any search value.');
    }
  }
  searchByEmail(email) {
    this.spinner.show();
    this.uploadService.getResumesAttachedByEmail(email).subscribe(res => {
      if (res) {
        this.spinner.hide();
        this.resumesData = res;
      } else {
        this.spinner.hide();
        this.alertService.error(res);
      }
    }, err => {
      this.spinner.hide();
      this.alertService.error(err);
    })
  }
  searchByFirstName(firstName) {
    this.spinner.show();
    this.uploadService.getResumesAttachedByFirstName(firstName).subscribe(res => {
      if (res) {
        this.spinner.hide();
        this.resumesData = res;
      } else {
        this.spinner.hide();
        this.alertService.error(res);
      }
    }, err => {
      this.spinner.hide();
      this.alertService.error(err);
    })
  }
  searchByLastName(lastName) {
    this.spinner.show();
    this.uploadService.getResumesAttachedByLastName(lastName).subscribe(res => {
      if (res) {
        this.spinner.hide();
        this.resumesData = res;
      } else {
        this.spinner.hide();
        this.alertService.error(res);
      }
    }, err => {
      this.spinner.hide();
      this.alertService.error(err);
    })
  }
  searchByDomain(domain) {
    this.spinner.show();
    this.uploadService.getResumesAttachedByDomain(domain).subscribe(res => {
      if (res) {
        this.spinner.hide();
        this.resumesData = res;
      } else {
        this.spinner.hide();
        this.alertService.error(res);
      }
    }, err => {
      this.spinner.hide();
      this.alertService.error(err);
    })
  }
  // select all resumes
  selectedUser(event, selectedValue) {
    console.log(selectedValue, event.target.checked);
    if (event.target.checked) {
      const resumeIdnew = { resumeId: selectedValue.resumeId };
      this.resumes.push(resumeIdnew);
    } else {
      this.resumes = this.resumes.filter(x => x.resumeId !== selectedValue.resumeId);
    }
    console.log(this.resumes);
  }
  // assign recriuter
  assignRecruiter() {
    const newOBJ = {
      "jobDescription": {
        "id": this.jdId
      },
      "resumes": this.resumes,
    }
    this.spinner.show();
    this.uploadService.assignResumeToJD(newOBJ).subscribe(res => {
      if (res) {
        this.spinner.hide();
        this.alertService.success('Assigned Successfully.');
        this.resumesData = [];
        this.searchByValue = '';
        this.searchBy = '';
        this.assignedResumes();
      } else {
        this.spinner.hide();
        this.alertService.error(res);
      }
    }, err => {
      this.spinner.hide();
      this.alertService.error(err);
    })
  }
  showDownloadBtn: boolean;
  // to shoe resume pdf/doc
  seeResumeDetails(resume) {
    this.spinner.show();
    this.uploadService.getResumeDetailsFile(resume['resumeId']).subscribe(res => {
      this.spinner.hide();

      let extensionType: string = '';
      if (resume['fileType'] === "pdf") {
        extensionType = "application/pdf";
      } else if (resume['fileType'] === "doc") {
        extensionType = "application/msword";
        extensionType = "application/pdf";
      } else if (resume['fileType'] === "docx") {
        extensionType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
      } else if (resume['fileType'] === "txt") {
        extensionType = "text/plain";
      } else if (resume['fileType'] === "odt") {
        extensionType = "application/vnd.oasis.opendocument.text";
      } else if (resume['fileType'] === "pages") {
        extensionType = "application/vnd.apple.pages";
      } else {
        extensionType = "application/octet-stream";
      }

      const blob: Blob = new Blob([res], { type: extensionType });
      const fileURL: string = window.URL.createObjectURL(blob);
      if (resume['fileType'] === "docx") {
        window.open(fileURL)
      } if (resume['fileType'] === "txt") {
        this.resumeLink = fileURL;
        // this.resumeLink = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
        this.showDownloadBtn = true;
      } else {  // pdf
        $("#showResume").modal("show"); // toggle, show, hide
        this.resumeLink = fileURL;
        // this.resumeLink = this.sanitizer.bypassSecurityTrustResourceUrl(fileURL);
      }
    }, err => {
      this.spinner.hide();
      this.alertService.error(err);
    })
  }
  downloadFile(resumeData: any){
    window.open(resumeData)
  }
  setpermission(){
    ;
    this.permissionData= this.authserv.receive();
    
    if(this.permissionData===undefined){
    this.authserv.setpermission2().subscribe((res)=>{
     this.permissionData =res;
     this.permissionData= this.authserv.send(this.permissionData);
   if( this.permissionData ){
   
   
   
    this.seeResume=this.permissionData.RESUME_VIEW,
    
    console.log("if  see resumeDetail",this.permissionData);
  }})
}
 else{
  
    
    this.seeResume=this.permissionData.RESUME_VIEW,
    this.spinner.hide();
    console.log("else see resumeDetail",this.permissionData);
  }
  
  }
  //RESUME_VIEW
}
