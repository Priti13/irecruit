import { Component, OnInit } from '@angular/core';
import { OffersService } from 'src/app/_service/offers.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-feedback-view',
  templateUrl: './feedback-view.component.html',
  styleUrls: ['./feedback-view.component.css']
})
export class FeedbackViewComponent implements OnInit {
  interviewId: string;
  rData: any = {};
  constructor(private offerService: OffersService, private route: ActivatedRoute) { }
  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.interviewId = params.get('id');
      console.log(this.interviewId);
      this.getFeedback(this.interviewId);
    });
  }
  getFeedback(interviewId) {
    this.offerService.getInterviewFeedback(interviewId).subscribe(
      res => {
        this.rData = res;
      }
    )
  }
}
