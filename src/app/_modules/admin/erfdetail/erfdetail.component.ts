import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ERFService } from 'src/app/_service/erf.service';
import { AlertService } from 'src/app/_service/alert.service';
import { SharedService } from 'src/app/_service/shared.service';
import { AuthenticationService } from 'src/app/_service/authentication.service';
declare var $: any;

@Component({
  selector: 'app-erfdetail',
  templateUrl: './erfdetail.component.html',
  styleUrls: ['./erfdetail.component.css']
})
export class ErfdetailComponent implements OnInit {
  optradio: any;
  erfDetails: any;
  show = false;
  public defaultModules = {
    toolbar: [
      ['bold', 'italic', 'underline', 'strike'], // toggled buttons
      ['blockquote', 'code-block'],

      [{ header: 1 }, { header: 2 }], // custom button values
      [{ list: 'ordered' }, { list: 'bullet' }],
      [{ script: 'sub' }, { script: 'super' }], // superscript/subscript
      [{ indent: '-1' }, { indent: '+1' }], // outdent/indent
      [{ direction: 'rtl' }], // text direction

      [{ size: ['small', false, 'large', 'huge'] }], // custom dropdown
      [{ header: [1, 2, 3, 4, 5, 6, false] }],

      [
        { color: [] },
        { background: [] }
      ], // dropdown with defaults from theme
      [{ font: [] }],
      [{ align: [] }],

      ['clean'], // remove formatting button

      ['link', 'image', 'video'] // link and image, video
    ]
  }

  // Priti
  selectCheckboxValue = false;
  userDetailsBySapId: any[][];
  userDetailsByUsername: any;
  assignBySelf: any;
  trackLeadList: any;
  // track lead form for adding lead
  //  trackLeadForm = {};
  trackLeadForm: AddTrackLead[] = [];
  id: any;

  // job description
  jobDesData: any[];
  jobDesForm: AddJD;
  sourceAddTrack: any;
  workAddTrack: any;
  budgetRemaining: number;
  positionsRemaining: number;

  selectedType: any;
  //shantam
  projectView: any;
  projectStatus: any;
  projectUpdate: any;
  trackLeadView: any;
  trackLeadUpdate: any;
  jobdescriptionView: any;
  jobdescriptionUpdate: any;
  jobdescriptionAdd: any;
  jobdescriptionDelete: any;
  permissionData: any;



  countryList: any[] = [];
  wageList: any[] = [];
  resourceAllocationList: any[] = [];
  // For Add TrackLead
  totalAssigningBudget: number;
  totalAssigningPosition: number;
  // For Edit Project Section
  editProForm: EditProject;
  sourceAddressesEdit: {
    country: { countryId: number, name: string, phonecode: number };
    state: { stateId: number, stateName: string };
    addressLineOne: string; addressLineTwo: string; zipCode: string;
  }[];
  workAddressesEdit: {
    country: { countryId: number, name: string, phonecode: number };
    state: { stateId: number, stateName: string };
    addressLineOne: string; addressLineTwo: string; zipCode: string;
  }[];
  businessHead: { name: string; buhead: string }[];
  stateList: any[][];
  stateListIndex: number;
  stateListWRK: any[][];
  stateListWRKIndex: number;
  gobalCountry: any = {
    country: { countryId: 0, name: '', phonecode: 0 },
    state: { stateId: 0, stateName: '' },
    addressLineOne: '',
    addressLineTwo: '',
    zipCode: ''
  }
  // For Remark Section
  remarkForm: any = { date: null, action: '', remark: '' };
  statusModel: string;
  trackleadDelete: any;
  trackUpdate: any;
  genderArray: any[];
  educationArray: any[];
  ethnicityArray: any[];
  constructor(
    private erfService: ERFService,
    private route: ActivatedRoute,
    private alertService: AlertService,
    private sharedService: SharedService,
    private router: Router,
    private authserv: AuthenticationService
  ) { }
  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
      this.getViewData(this.id);
      this.permissionStatus();
    });
    this.assignBySelf = JSON.parse(localStorage.getItem('currentUser'));
    this.initiateTrackForm();
    this.intialiseEditProject();
    this.allWorkAndSourceAddress();
  }
  onChange(event) {
    this.selectedType = event.target.value;
  }
  // get details erf by id
  getViewData(id) {
    this.erfService.getErfDetail(id).subscribe(res => {
      if (res['approved'] !== 'ACTIVE')
        this.router.navigate(['/view-project']);
      this.erfDetails = res;
      this.statusModel = res['projectStatus'];
    }, error => {
      this.alertService.error(error);
    });
  }
  // intialise
  initiateTrackForm() {
    this.trackLeadForm = [{
      id: 0,
      domainsArray: [],
      sapId: null,
      budget: null,
      position: null,
      erf: { erfId: this.id, },
      user: { id: null, username: '' },
      isBudgetExceed: false,
      isPositionExceed: false,
      selectCheckboxValue: false,
    }];
    this.jobDesForm = {
      id: 0,
      erf: { erfId: this.id, },
      noOfEmployee: null,
      minExprience: null,
      maxExprience: null,
      jdName: '',
      domain: '',
      budget: null,
      visa: 'no',
      visaCountry: '',
      workLocation: '',
      sourceLocation: '',
      body: '',
      additionalDetail: '',
      additionalCategory: [],
      currencyType: '',
      wageType: '',
      resourceAllocation: '',
      requisitionCategory: '',
      jobType: '',
      jobDuration: null,
      competenceLevel: '',
      joiningLocation: '',
      nationality: '',
      shiftTiming: '',
      gender: {},
      ethnicity: {},
      educationType: {},
      education: []
    };
  }
  addTrackLeadObject() {
    const newObj = {
      id: 0,
      domainsArray: [],
      sapId: null,
      budget: null,
      position: null,
      erf: { erfId: this.id, },
      user: { id: null, username: '' },
      isBudgetExceed: false,
      isPositionExceed: false,
      selectCheckboxValue: false,
    };
    this.trackLeadForm.push(newObj);
  }
  removeTrackLeadObject(i: number) {
    this.trackLeadForm.splice(i, 1);
    this.TrackLeadFormValidate();
  }
  // add empl id
  showEmployeeID(user, index) {
    for (let i = 0; i < this.userDetailsByUsername.length; i++) {
      if (user === this.userDetailsByUsername[i].username) {
        this.trackLeadForm[index].sapId = this.userDetailsByUsername[i].sapId;
        this.trackLeadForm[index].user.id = this.userDetailsByUsername[i].id;
      }
    }
  }
  // get user details by sapid
  getUserBySapID(id) {
    this.erfService.getSearchUserBySapId(id).subscribe((res: any[]) => {
      this.userDetailsBySapId = res;
      console.log(this.userDetailsBySapId);
    });
  }
  // add empl id
  showuserName(user, index) {
    console.log(user);
    /* for (let i = 0; i < this.userDetailsBySapId.length; i++) {
      if (user === this.userDetailsBySapId[i]['sapId']) {
        this.trackLeadForm[index].user.username = this.userDetailsBySapId[i]['username'];
        this.trackLeadForm[index].user.id = this.userDetailsBySapId[i]['id'];
      }
    } */
    this.userDetailsBySapId.forEach(element => {
      if (user === element['sapId']) {
        this.trackLeadForm[index].user.username = element['username'];
        this.trackLeadForm[index].user.id = element['id'];
      }
    })
  }
  // get user details by username
  getUserByUserName(name) {
    console.log(name);
    if (name) {
      this.erfService.getSearchUserByUsername(name).subscribe(res => {
        this.userDetailsByUsername = res;
        console.log(this.userDetailsByUsername);
      });
    }

  }
  // checkbox change
  toggleVisibility(e, i) {
    this.trackLeadForm[i].selectCheckboxValue = e.target.checked;
    if (this.trackLeadForm[i].selectCheckboxValue) {
      this.trackLeadForm[i].sapId = this.assignBySelf.sapId;
      this.trackLeadForm[i].user.username = this.assignBySelf.name;
      this.trackLeadForm[i].user.id = this.assignBySelf.id;
    } else {
      this.trackLeadForm[i].sapId = null;
      this.trackLeadForm[i].user.id = null;
      this.trackLeadForm[i].user.username = '';
    }
  }
  getDetailTrackLead() {
    this.erfService.trackLeadDetails(this.id).subscribe(res => {
      this.trackLeadList = res['getTrack'];
      this.budgetRemaining = res['budgetRemaining'];
      this.positionsRemaining = res['positionsRemaining'];
    }), error => {
      this.alertService.error(error);
    };
  }
  clearTrackLeadForm() {
    this.initiateTrackForm();
  }

  TrackLeadFormValidate() {
    this.totalAssigningBudget = 0;
    this.totalAssigningPosition = 0;
    this.trackLeadForm.forEach((item, index) => {
      this.totalAssigningPosition += item.position
      this.totalAssigningBudget += (item.budget * item.position)

      if (this.totalAssigningBudget > this.budgetRemaining) {
        item.isBudgetExceed = true
      } else {
        item.isBudgetExceed = false
      }

      if (this.totalAssigningPosition > this.positionsRemaining) {
        item.isPositionExceed = true
      } else {
        item.isPositionExceed = false
      }
    })
  }
  submitTrackLeadForm() {
    this.TrackLeadFormValidate()
    if (this.totalAssigningBudget > this.budgetRemaining || this.totalAssigningPosition > this.positionsRemaining) {
      return this.alertService.error('Enter right value.');
    } else {
      const trackLeadFrm: any[] = this.trackLeadForm.map(e => {
        return {
          'user': e.user,
          'domainsArray': e.domainsArray,
          'budget': e.budget,
          'position': e.position,
          'erf': e.erf,
          'currencyType': this.erfDetails.currency,
        };
      })
      this.erfService.addTrack(trackLeadFrm).subscribe(res => {
        this.getDetailTrackLead();
        this.initiateTrackForm();
        this.alertService.success('Submittted Successfully!');
        $("#addAssignModal").modal("hide");
      }, error => {
        this.alertService.error(error);
        this.alertService.error('Please fill all Correct Value!');
      });
    }
    //window.location.reload();
  }
  onShow() {
    this.show = !this.show;
  }
  public onSelect(item) {
  }
  // job description start here
  jobDescriptionData() {
    this.erfService.jobDescriptionDetails(this.id).subscribe(res => {
      this.jobDesData = res as any[];
    }), error => {
      this.alertService.error(error);
    };
    this.allGenderEducationEthnicity();
  }
  allGenderEducationEthnicity() {
    this.erfService.getGenderEducationEthnicity().subscribe(res => {
      console.log('jjg');
      this.genderArray = res['genderData'] as any[];
      this.educationArray = res['educationType'] as any[];
      this.ethnicityArray = res['ethnicity'] as any[];
    }), error => {
      this.alertService.error(error);
    };
  }

  allWorkAndSourceAddress() {
    this.erfService.getAllAdrress(this.id).subscribe(res => {
      this.sourceAddTrack = res['sourceAddress'];
      this.workAddTrack = res['workAddress'];
    }), error => {
      this.alertService.error(error);
    };
    this.sharedService.getCountryList().subscribe(
      (res: any[]) => this.countryList = res
    )
    this.sharedService.getWageList().subscribe(
      (res: any[]) => this.wageList = res
    )
    this.sharedService.getResourceAllocation().subscribe(
      (res: any[]) => this.resourceAllocationList = res
    )
  }
  addJobDescriptionSubmit() {
    if (this.jobDesForm.visa == 'yes') {
      this.jobDesForm.visa = true;
    }
    else {
      this.jobDesForm.visa = false;
      delete this.jobDesForm.visaCountry;
    }
    delete this.jobDesForm.gender.gender;
    delete this.jobDesForm.ethnicity.ethnicity;
    delete this.jobDesForm.educationType.educationType;
    const education = this.jobDesForm.education;
    this.jobDesForm.education = [education];
    const newObj = [this.jobDesForm];
    console.log(newObj)

    this.erfService.addJobDesNew(newObj).subscribe(res => {
      if (res) {
        if (res['status'] === 'NOT_ACCEPTABLE') {
          this.alertService.error(res['message']);
        } else {
          document.getElementById('addJobModal').click();
          this.jobDescriptionData();
          this.initiateTrackForm();
          this.alertService.success('Submittted Successfully!');
        }
      } else {
        this.alertService.error(res['message']);
      }
    }, error => {
      this.alertService.error(error);
    });
  }
  // For Edit Project Section
  intialiseEditProject() {
    this.stateList = [];
    this.stateListIndex = 0;
    this.stateListWRK = [];
    this.stateListWRKIndex = 0;
    this.editProForm = {
      buhead: [],
      totalPosition: null,
      maxBudget: null,
      projectDuration: null,
    }
    this.sourceAddressesEdit = [{
      country: { countryId: 0, name: '', phonecode: 0 },
      state: { stateId: 0, stateName: '' },
      addressLineOne: '',
      addressLineTwo: '',
      zipCode: '',
    }];
    this.workAddressesEdit = [{
      country: { countryId: 0, name: '', phonecode: 0 },
      state: { stateId: 0, stateName: '' },
      addressLineOne: '',
      addressLineTwo: '',
      zipCode: '',
    }];
    this.businessHead = [];
    // { name: '', buhead: '' }
  }
  editProjectDetails() {
    for (let i = 0; i < this.businessHead.length; i++) {
      this.editProForm.buhead[i] = this.businessHead[i].buhead;
    }
    this.editProForm['sourceAddresses'] = this.sourceAddressesEdit;
    this.editProForm['workAddresses'] = this.workAddressesEdit;

    this.erfService.editProject(this.editProForm, this.id).subscribe(res => {
      this.getViewData(this.id);
      $("#editproject").modal("hide");
      this.intialiseEditProject();
      this.alertService.success('Submittted Successfully!');
    }, error => {
      this.alertService.error(error);
      this.alertService.error('Please fill all Correct Value!');
    });
  }
  editProjectDetailsMod(erfDetails) {
    this.editProForm.projectDuration = erfDetails.projectDuration;
    this.editProForm.maxBudget = erfDetails.maxBudget;
    this.editProForm.totalPosition = erfDetails.totalPosition;
    for (let i = 0; i < erfDetails.buhead.length; i++) {
      this.editProForm.buhead.push(erfDetails.buhead[i].sapId)
      this.businessHead.push({ name: erfDetails.buhead[i].name, buhead: erfDetails.buhead[i].sapId })
    }
    this.sourceAddressesEdit = erfDetails.sourceAddresses.map((e, index) => {
      let temp: any = { country: '', state: '' };
      temp.country = this.countryList.find(r => r.name === e.country);
      this.sharedService.getStateList(temp.country.countryId).subscribe((res: any[]) => {
        this.stateList.push(res);
        this.stateListIndex++;
        temp.state = res.find(r => r.stateName === e.state);
      });

      return temp;
    });
    this.workAddressesEdit = erfDetails.workAddresses.map((e, index) => {
      let temp: any = { country: '', state: '', addressLineOne: e.addressLineOne, addressLineTwo: e.addressLineTwo, zipCode: e.zipCode };
      temp.country = this.countryList.find(r => r.name === e.country);
      this.sharedService.getStateList(temp.country.countryId).subscribe((res: any[]) => {
        this.stateListWRK.push(res);
        this.stateListWRKIndex++;
        temp.state = res.find(r => r.stateName === e.state);
      });
      return temp;
    });
  }
  deletebusinessHead(newObj) {
    this.businessHead = this.businessHead.filter(obj => obj !== newObj);
  }
  addbusinessHead() {
    const newValue = { name: '', buhead: '' }
    this.businessHead.push(newValue);
  }
  getUserByUserNameEdit(name) {
    console.log(name);
    if (name) {
      this.erfService.getSearchUserByUsername(name).subscribe(res => {
        this.userDetailsByUsername = res;
        console.log(this.userDetailsByUsername);
      });
    }
  }
  showEmployeeIDEdit(user, index) {
    for (let i = 0; i < this.userDetailsByUsername.length; i++) {
      if (user === this.userDetailsByUsername[i].username) {
        this.businessHead[index].buhead = this.userDetailsByUsername[i].sapId;
      }
    }
  }
  addSourceAddressEdit() {
    const newValue = {
      country: { countryId: 0, name: '', phonecode: 0 },
      state: { stateId: 0, stateName: '' },
      addressLineOne: '',
      addressLineTwo: '',
      zipCode: '',
    }
    this.sourceAddressesEdit.push(newValue);
    this.stateListIndex++;
  }
  onCountryChangeSRC(selectedCountry, index) {
    this.getStateByCountry(selectedCountry.value.countryId, index);
  }
  getStateByCountry(countryId, index) {
    this.sharedService.getStateList(countryId).subscribe(res => {
      this.stateList[index] = res as [];
    });
  }
  deleteSourceAddressEdit(newObj) {
    this.sourceAddressesEdit = this.sourceAddressesEdit.filter(obj => obj !== newObj);
  }
  addWorkAddressEdit() {
    const newValue = {
      country: { countryId: 0, name: '', phonecode: 0 },
      state: { stateId: 0, stateName: '' },
      addressLineOne: '',
      addressLineTwo: '',
      zipCode: '',
    }
    this.workAddressesEdit.push(newValue);
    this.stateListWRKIndex++;
  }
  onCountryChangeWRK(selectedCountry, index) {
    this.getStateByCountryWRK(selectedCountry.value.countryId, index);
  }
  getStateByCountryWRK(countryId, index) {
    this.sharedService.getStateList(countryId).subscribe(res => {
      this.stateListWRK[index] = res as [];
    });
  }
  deleteWorkAddressEdit(newObj) {
    this.workAddressesEdit = this.workAddressesEdit.filter(obj => obj !== newObj);
  }
  // For Project Status Section
  onStatusChange(event) {
    let status: string = event.target.value;
    // if (status !== 'ONHOLD' && status !== 'CLOSE' && status !== 'OPEN') return;
    this.remarkForm['date'] = new Date();
    this.remarkForm['action'] = status;
    $("#remark").modal("show");
  }
  undoStatusChange() {
    this.statusModel = this.erfDetails['projectStatus'];
    $("#remark").modal("hide");
  }
  submitRemark() {
    this.erfService.setRemark(this.remarkForm, this.id).subscribe(res => {
      if (res['status']) {
        this.erfDetails['projectStatus'] = res['action'];
      } else {
        this.statusModel = this.erfDetails['projectStatus'];
      }

      $("#remark").modal("hide");
      this.alertService.success(res['message']);
    });
  }
  permissionStatus() {
    this.permissionData = this.authserv.receive();
    if (this.permissionData === undefined) {
      this.authserv.setpermission2().subscribe((res) => {
        this.permissionData = res;
        this.permissionData = this.authserv.send(this.permissionData);
        if (this.permissionData) {
          console.log("erfdetail", this.permissionData);
          this.projectUpdate = this.permissionData.PROJECT_UPDATE;
          this.projectView = this.permissionData.PROJECT_VIEW;
          this.trackLeadView = this.permissionData.TRACKLEAD_VIEW;
          this.trackLeadUpdate = this.permissionData.TRACKLEAD_ADD;
          this.jobdescriptionView = this.permissionData.JOBDESCRIPTION_VIEW;
          this.jobdescriptionDelete = this.permissionData.JOBDESCRIPTION_DELETE;
          this.jobdescriptionUpdate = this.permissionData.JOBDESCRIPTION_UPDATE;
          this.jobdescriptionAdd = this.permissionData.JOBDESCRIPTION_ADD;
          this.trackleadDelete = this.permissionData.TRACKLEAD_DELETE;
          this.trackUpdate = this.permissionData.TRACKLEAD_UPDATE;
          this.projectStatus = this.permissionData.PROJECT_STATUS;
        }
      })
    }

    else {

      this.projectUpdate = this.permissionData.PROJECT_UPDATE;
      this.projectView = this.permissionData.PROJECT_VIEW;
      this.trackLeadView = this.permissionData.TRACKLEAD_VIEW;
      this.trackLeadUpdate = this.permissionData.TRACKLEAD_ADD;
      this.jobdescriptionView = this.permissionData.JOBDESCRIPTION_VIEW;
      this.jobdescriptionDelete = this.permissionData.JOBDESCRIPTION_DELETE;
      this.jobdescriptionUpdate = this.permissionData.JOBDESCRIPTION_UPDATE;
      this.jobdescriptionAdd = this.permissionData.JOBDESCRIPTION_ADD;
      this.projectStatus = this.permissionData.PROJECT_STATUS;
      console.log("erfdetail", this.permissionData);
    }
  }

}
class AddJD {
  noOfEmployee: number;
  minExprience: number;
  maxExprience: number;
  jdName: string;
  domain: string;
  budget: number;
  visa: any;
  visaCountry: any;
  workLocation: any;
  sourceLocation: any;
  body: string;
  additionalDetail: string;
  additionalCategory: string[]
  currencyType: string;
  wageType: any;
  resourceAllocation: any;
  requisitionCategory: string;
  jobType: string;
  jobDuration: number;
  competenceLevel: string;
  joiningLocation: any;
  id: number = 0;
  erf: any;
  nationality: string;
  shiftTiming: string;
  gender: any;
  ethnicity: any;
  educationType: any;
  education: any
}
interface AddTrackLead {
  id: number;
  domainsArray: string[];
  sapId: any,
  budget: number;
  position: number;
  user: { id: number, username: string };
  erf: { erfId: number };
  isBudgetExceed: boolean;
  isPositionExceed: boolean;
  selectCheckboxValue: boolean;
}
interface EditProject {
  buhead: string[];
  totalPosition: number;
  maxBudget: number;
  projectDuration: string;
}