import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ErfdetailComponent } from './erfdetail.component';

describe('ErfdetailComponent', () => {
  let component: ErfdetailComponent;
  let fixture: ComponentFixture<ErfdetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ErfdetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErfdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
