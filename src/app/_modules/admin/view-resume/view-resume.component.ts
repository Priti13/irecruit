import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JdService } from 'src/app/_service/jd.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { AlertService } from 'src/app/_service/alert.service';
import { UploadService } from 'src/app/_service/upload.service';
import { DomSanitizer } from '@angular/platform-browser';
import {AuthenticationService} from 'src/app/_service/authentication.service';
declare var $: any;

@Component({
  selector: 'app-view-resume',
  templateUrl: './view-resume.component.html',
  styleUrls: ['./view-resume.component.css']
})
export class ViewResumeComponent implements OnInit {
  jdId: string;
  resumes: any[] = [];
  showResume: any = {};
  show = false;
  resumeLink: any;
  //shantam
  calenderView:any;
  feedBackView:any;
  permissionData:any;
  Panel_view: any;

  constructor(private route: ActivatedRoute, private jdService: JdService, public sanitizer: DomSanitizer,
    private spinner: NgxSpinnerService, private alertService: AlertService, private uploadService: UploadService,
    private authserv:AuthenticationService) { }
  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.jdId = params.get('id')
    });
    this.jdService.getResumesAttached(this.jdId).subscribe((r: any[]) => {
      this.resumes = r
    })
    this.setpermission();
  }
  onShow(resume: any) {
    this.showResume = resume.resumeData['resumeId']
    
    if (!this.show) {
      console.log("resume"+JSON.stringify(resume));
      this.spinner.show();
      this.uploadService.getResumeDetailsFile(this.showResume).subscribe(res => {
        this.spinner.hide();
      
        let extensionType: string = '';
        if (resume['resumeData']['fileType'] === "pdf") {
          extensionType = "application/pdf";
        } else if (resume['resumeData']['fileType'] === "doc") {
          extensionType = "application/msword";
          extensionType = "application/pdf";
        } else if (resume['resumeData']['fileType'] === "docx") {
          extensionType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
        } else if (resume['resumeData']['fileType'] === "txt") {
          extensionType = "text/plain";
        } else if (resume['resumeData']['fileType'] === "odt") {
          extensionType = "application/vnd.oasis.opendocument.text";
        } else if (resume['resumeData']['fileType'] === "pages") {
          extensionType = "application/vnd.apple.pages";
        } else {
          extensionType = "application/octet-stream";
        }

        const blob: Blob = new Blob([res], { type: extensionType });
        const fileURL: string = window.URL.createObjectURL(blob);

        if (resume['resumeData']['fileType'] === "pdf") {
          window.open(fileURL);
          console.log("resumeDARTT"+res);
        } else {  // pdf, txt
          this.resumeLink = fileURL;
          this.show = !this.show;
          console.log("notresumeDARTT"+res);
        }
      }, err => {
        this.spinner.hide();
        this.alertService.error(err);
      })
    } else this.show = !this.show;
  }
  setpermission(){
   
    this.permissionData= this.authserv.receive();
    
    if(this.permissionData===undefined){
    this.authserv.setpermission2().subscribe((res)=>{
     this.permissionData =res;
     this.permissionData= this.authserv.send(this.permissionData);
   if( this.permissionData ){
    
   
    this.calenderView=this.permissionData['CALENDAR_VIEW'],
    this.feedBackView=this.permissionData.FEEDBACK_VIEW,
   this.Panel_view= this.permissionData.PANEL_VIEW;
    console.log("viewresume",this.permissionData);
  }})
}

  else{
  
    this.calenderView=this.permissionData['CALENDAR_VIEW'],
    this.feedBackView=this.permissionData.FEEDBACK_VIEW,
    this.Panel_view= this.permissionData.PANEL_VIEW;
    
    console.log("viewresume",this.permissionData);
  }
   /* return this.authserv.permission2().subscribe(res=>{
      this.calenderView=res.CALENDAR_VIEW;
      this.feedBackView=res.FEEDBACK_VIEW;
      console.log(this.calenderView,this.feedBackView);
    });*/
  }
}
