import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/_service/authentication.service';
import { CurrentUser } from 'src/app/_model/user';
import { NgxSpinnerService } from 'ngx-spinner';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css']
})
export class SideBarComponent implements OnInit {
  mypermission: any;
  currentUserValue: CurrentUser;
  token="eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI0NCIsImlhdCI6MTU5MjIwMDM4MCwic3ViIjoiODAgMiAyIiwiaXNzIjoic3ViaGFzaDAzOTdAZ21haWwuY29tIiwiZXhwIjozMTg0NDAwNzYxfQ.eWZ3pLQ4W6UvFnzYrt1OQCG-4quRjr7t2fZ4de1ijuo";
projectGenerate:any;
permissionData:any
ErfAction:any;
assignRecruiter:any;
recruiterasssigntoJd:any;


  constructor( private mypermissionService: AuthenticationService,
    private spinner: NgxSpinnerService,private http: HttpClient) {

  }

  getPermission() {
    this.mypermission = this.mypermissionService.currentUserValue.permissions;
    //console.log('Response', this.mypermission);
  }

  ngOnInit() {
    this.getPermission();
   this.permissionstatus();
  }
permissionstatus(){
 // window.location.reload();
  this.permissionData=this.mypermissionService.receive();
  if( this.permissionData===undefined){
 this.mypermissionService.setpermission2().subscribe(res=>{
   this.permissionData=res;
   this.mypermissionService.send(this.permissionData);
   this.projectGenerate=this.permissionData['PROJECT_GENERATE'],
        this.ErfAction=this.permissionData.ERF_ACTION,
        this.assignRecruiter=this.permissionData.RECRUITER_ASSIGN,
      this.recruiterasssigntoJd = this.permissionData.CANDIDATE_ASSIGN_TO_JD;
      // console.log("if sideapi",this.permissionData);
 });
}
 else{
  this.projectGenerate=this.permissionData['PROJECT_GENERATE'],
  this.ErfAction=this.permissionData.ERF_ACTION,
  this.assignRecruiter=this.permissionData.RECRUITER_ASSIGN,
this.recruiterasssigntoJd = this.permissionData.CANDIDATE_ASSIGN_TO_JD;
// console.log("else sidebar",this.permissionData);
 }

  /*this.permissionData= this.mypermissionService.setpermission();
  if(this.permissionData===undefined){
    this.spinner.show();
  
  this.http.get<any>(environment.url +`irecruit-api/user/set-dynamic-permission`).subscribe((res)=>{
    this.permissionData =res,
      console.log("if sideapi",this.permissionData);
      if(this.permissionData){
        this.projectGenerate=this.permissionData['PROJECT_GENERATE'],
        this.ErfAction=this.permissionData.ERF_ACTION,
        this.assignRecruiter=this.permissionData.RECRUITER_ASSIGN,
      this.recruiterasssigntoJd = this.permissionData.CANDIDATE_ASSIGN_TO_JD;
      }
      });
      

  }*/
  /*else{
    this.spinner.hide();
    this.projectGenerate=this.permissionData['PROJECT_GENERATE'],
    this.ErfAction=this.permissionData.ERF_ACTION,
    this.assignRecruiter=this.permissionData.RECRUITER_ASSIGN,
  this.recruiterasssigntoJd = this.permissionData.CANDIDATE_ASSIGN_TO_JD;
    console.log("else sidebar",this.permissionData);
  }*/
 
 
}
 
}

