import { Component, OnInit } from '@angular/core';
import { OffersService } from 'src/app/_service/offers.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'src/app/_service/alert.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css']
})
export class FeedbackComponent implements OnInit {
  jdId: string;
  resumeId: string;
  interviewId: string;
  interviewDetails: any = {};
  feedbackForm: FeedbackForm;
  today: any;

  constructor(private offerService: OffersService, private route: ActivatedRoute,
    private alertService: AlertService, private router: Router) { }
  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.resumeId = params.get('resumeId');
      this.jdId = params.get('jdId');
      this.interviewId = params.get('interviewId');
      this.getFeedbackDetails();
      this.initMethod();
    });
  }
  getFeedbackDetails() {
    this.offerService.getOffersInitDtails(this.jdId, this.resumeId, this.interviewId).subscribe(
      res => {
        this.interviewDetails = res;
      }, error => {
        this.alertService.error(error);
      }
    )
  }
  initMethod() {
    this.feedbackForm = {
      applicantName: '',
      feedbackDate: null,
      interviewType: '',
      skillTrack: [],
      communicationSkills: '',
      culturalFitment: '',
      skill: [],
      multiSkilled: '',
      supervisorSkill: '',
      totalExprience: null,
      relevantExprience: null,
      comment: '',
      result: '',
      otherTechnology: '',
      otherRole: [],
      newTechnologies: [],
      activeRound: [],
      inactiveRound: [0],
      interviewDetail: {
        id: parseInt(this.interviewId)
      }
    }
  }
  getSkillTrack(skillTrackArr: string[]) {
    console.log(skillTrackArr);
    const lastElem: string = skillTrackArr[skillTrackArr.length - 1];
    this.feedbackForm.skill.push({
      skillNumber: (skillTrackArr.length - 1),
      skillName: lastElem,
      skillRating: 0
    })
  }
  SkillRatingformChanged(item) {
    item.skillRating = parseInt(item.skillRating);
  }
  submitFeedback(myForm: NgForm) {
    console.log(this.feedbackForm)
    console.log("this.feedbackForm", this.feedbackForm);
    // console.log(new Date())
    // if (!myForm.valid) {
    //   return;
    // }
    this.feedbackForm.applicantName = this.interviewDetails.applicant
    this.feedbackForm.interviewType = this.interviewDetails.interviewType
    //this.feedbackForm.feedbackDate = new Date();
    //
    this.today = new Date();
    var dd = this.today.getDate();
    var mm = this.today.getMonth() + 1;
    var yyyy = this.today.getFullYear();
    var HH = this.today.getHours();
    var MM = this.today.getMinutes();
    if (mm >= 10) {
      mm = mm;
    }
    else {
      mm = '0' + mm;
    }
    if (dd >= 10) {
      dd = dd;
    }
    else {
      dd = '0' + dd;
    }
    if (MM >= 10) {
      MM = MM;
    }
    else {
      MM = '0' + MM;
    }
    this.today = yyyy + '-' + mm + '-' + dd + ' ' + HH + ':' + MM;
    this.feedbackForm.feedbackDate = this.today;
    console.log("this.feedbackForm", this.feedbackForm);
    //console.log("this.feedbackForm.feedbackDate", this.feedbackForm.feedbackDate);
    this.offerService.setInterviewFeedback(this.feedbackForm).subscribe(
      res => {
        console.log(res)
        this.initMethod();
        this.router.navigate(['/set-feedback', this.jdId, this.resumeId])
      }, error => {
        this.alertService.error(error);
      }
    )
  }
}

interface FeedbackForm {
  applicantName: string;
  feedbackDate: Date;
  interviewType: string;
  skillTrack: string[];
  communicationSkills: string;
  culturalFitment: string;
  skill: {
    skillNumber: number,
    skillName: string,
    skillRating: number
  }[];
  multiSkilled: string;
  supervisorSkill: string;
  totalExprience: number;
  relevantExprience: number;
  comment: string;
  result: string;
  otherTechnology: string;
  otherRole: number[];
  newTechnologies: string[];
  activeRound: number[]; // empty
  inactiveRound: number[];      // 0
  interviewDetail: {
    id: number
  }
}
