import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { OffersService } from 'src/app/_service/offers.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertService } from 'src/app/_service/alert.service';

@Component({
  selector: 'app-externalfeedback',
  templateUrl: './externalfeedback.component.html',
  styleUrls: ['./externalfeedback.component.css']
})
export class ExternalfeedbackComponent implements OnInit {

  jdId: string;
  resumeId: string;
  interviewId: string;
  interviewDetails: any = {};
  feedbackForm: FeedbackForm;
  today: any;
  token:any;

  constructor(private offerService: OffersService, private route: ActivatedRoute,
    private alertService: AlertService, private router: Router) { }
  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.token = params.get('token');
    
    });
    
    this.initMethod();
   /* this.route.paramMap.subscribe(params => {
      this.resumeId = params.get('resumeId');
      this.jdId = params.get('jdId');
      this.interviewId = params.get('interviewId');
      this.getFeedbackDetails();
      this.initMethod();
    });*/
  }
 /* getFeedbackDetails() {
    this.offerService.getOffersInitDtails(this.jdId, this.resumeId, this.interviewId).subscribe(
      res => {
        this.interviewDetails = res;
      }, error => {
        this.alertService.error(error);
      }
    )
  }*/
  initMethod() {
    this.feedbackForm = {
      token:this.token,
      applicantName: 'shantam',
      feedbackDate: "2019-04-02 11:45",
      interviewType: 'Online',
      skillTrack: [],
      communicationSkills: '',
      culturalFitment: '',
      skill: [],
      multiSkilled: '',
      supervisorSkill: '',
      totalExprience: null,
      relevantExprience: null,
      comment: '',
      result: '',
      otherTechnology: 'yes',
     // otherRole:["yes"],
      newTechnologies:["angular","c++"]
      
    }
  }
  getSkillTrack(skillTrackArr: string[]) {
    const lastElem: string = skillTrackArr[skillTrackArr.length - 1];
    this.feedbackForm.skill.push({
      skillNumber: (skillTrackArr.length - 1),
      skillName: lastElem,
      skillRating: 0
    })
    //console.log("skill track", this.feedbackForm.skill);
  }
  SkillRatingformChanged(item) {
    item.skillRating = parseInt(item.skillRating);
  }
  submitFeedback(myForm: NgForm) {
    console.log(this.feedbackForm)
    //console.log("this.feedbackForm.feedbackDate", this.feedbackForm.feedbackDate);
    // console.log(new Date())
    if (!myForm.valid) {
      return;
    }
   // this.feedbackForm.applicantName = this.interviewDetails.applicant
    //this.feedbackForm.interviewType = this.interviewDetails.interviewType
    //this.feedbackForm.feedbackDate = new Date();
    //
   /* this.today = new Date();
    var dd = this.today.getDate();
    var mm = this.today.getMonth() + 1;
    var yyyy = this.today.getFullYear();
    var HH = this.today.getHours();
    var MM = this.today.getMinutes();
    if (mm >= 10) {
      mm = mm;
    }
    else {
      mm = '0' + mm;
    }
    if (dd >= 10) {
      dd = dd;
    }
    else {
      dd = '0' + dd;
    }
    this.today = yyyy + '-' + mm + '-' + dd + ' ' + HH + ':' + MM;
    this.feedbackForm.feedbackDate = this.today;*/
    //
    //console.log("this.feedbackForm.feedbackDate", this.feedbackForm.feedbackDate);
   // this.offerService.setInterviewFeedback(this.feedbackForm).subscribe(
    this.offerService.setExternalFeedback(this.feedbackForm).subscribe(
   res => {
        console.log("Shnamm",res)
        this.initMethod();
        this.alertService.success("Sucessfully done ");
       // this.router.navigate(['/set-feedback', this.jdId, this.resumeId])
      }, error => {
        this.alertService.error(error);
      }
    )
  }
}

interface FeedbackForm {
  token:string;
  applicantName: string;
  feedbackDate: string;
  interviewType: string;
  skillTrack: string[];
  communicationSkills: string;
  culturalFitment: string;
  skill: {
    skillNumber: number,
    skillName: string,
    skillRating: number
  }[];
  multiSkilled: string;
  supervisorSkill: string;
  totalExprience: number;
  relevantExprience: number;
  comment: string;
  result: string;
  otherTechnology: string;
  //otherRole: string[];
  newTechnologies: string[];
  //activeRound: number[]; // empty
  //inactiveRound: number[];      // 0
  //interviewDetail: {
   // id: number
 // }
}
/*"token":"eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiI0NCIsImlhdCI6MTU5MjIwMDM4MCwic3ViIjoiODAgMiAyIiwiaXNzIjoic3ViaGFzaDAzOTdAZ21haWwuY29tIiwiZXhwIjozMTg0NDAwNzYxfQ.eWZ3pLQ4W6UvFnzYrt1OQCG-4quRjr7t2fZ4de1ijuo",   
        "applicantName":"Arti Yadav",
        "feedbackDate":"2019-04-02 11:45",
        "interviewType":"ONLINE",
        "skillTrack":["database","java","front end developer"],
            "communicationSkills":"sdgfds",
            "culturalFitment":"gdfdfgf",
            "skill":[
                {
                    "skillNumber":1,
                    "skillName":"database",
                    "skillRating":5
                },
                {
                  "skillNumber":2,
                  "skillName":"C++",
                  "skillRating":4
                },
                {
                  "skillNumber":3,
                  "skillName":"C",
                  "skillRating":3
                }
            ],
            "multiSkilled":"dsgfgh",
            "supervisorSkill":"sdfse",
            "totalExprience":1,
            "relevantExprience":2,
            "comment":"This is feedback form which will be fillup by eveluator",
            "result":"yes",
            "otherTechnology":"yes",
            "newTechnologies":["angular","c++"]
    } */