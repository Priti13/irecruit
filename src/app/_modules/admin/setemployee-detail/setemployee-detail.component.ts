import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AssignRecruiterService} from 'src/app/_service/assign-recruiter.service';

@Component({
  selector: 'app-setemployee-detail',
  templateUrl: './setemployee-detail.component.html',
  styleUrls: ['./setemployee-detail.component.css']
})
export class SetemployeeDetailComponent implements OnInit {
  userId: any;
  Userdetail:any;
  updateUserDet:any;
  band:any;
  subBand:any;
  jobtitle:any;
  department:any;
  ItApplication:any;
entityId:any;

countryId:any;
currentLocation:any;
employeestatus:any;

resouceAllocationId:any;
maritalStatus:any;
supervisorId:any;
matrixManagerId:any;

projectId:any;
buHrId:any;
editEmployeeentity:any;
editEmployeeProject:any;
editEmployeeCountryId:any;
editEmployeeResourceAllocation:any;


  constructor( private route: ActivatedRoute, private assignReccruiserv: AssignRecruiterService) {
    this.route.paramMap.subscribe(params => {
      this.userId = params.get('id');
    
    });
   
  }

  ngOnInit() {
this.GetUserDetail();
//this.updateUserProfile();

  }
  GetUserDetail(){
    return this.assignReccruiserv.getUserDetail(this.userId).subscribe((res)=>{
this.Userdetail=res
//console.log(this.Userdetail);
    })
  }
  updateUserProfile(){
    const Body = {
      "band": this.band, "subBand": this.subBand, "jobTitle": this.jobtitle, "department": this.department, "entity": {
        "entityId": this.entityId
      }, "baseLocation": { "countryId": this.countryId }, "currentLocation": this.currentLocation, "employeeStatus": this.employeestatus,
      "resourceAllocation": { "resouceAllocationId": this.resouceAllocationId }, "maritalStatus": this.maritalStatus, "supervisorId": this.supervisorId,
      "matrixManagerId": this.matrixManagerId, "project": { "projectId": this.projectId }, "buHrId": this.buHrId
    }
    return this.assignReccruiserv.updateUserProfile(this.userId,Body).subscribe((res)=>{
      this.updateUserDet=res;
      console.log("BODY",Body);
      //console.log("nm",res)});
     
  });

}
editemployeedetail(){
  return this.assignReccruiserv.editpermission().subscribe((res)=>{
    this.editEmployeeentity=res['entities'];
    this.editEmployeeCountryId=res.countries;
    this.editEmployeeProject=res.projects;
    this.editEmployeeResourceAllocation =res.resourceAllocations;

    console.log(res)})
}
}
