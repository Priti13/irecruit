import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetemployeeDetailComponent } from './setemployee-detail.component';

describe('SetemployeeDetailComponent', () => {
  let component: SetemployeeDetailComponent;
  let fixture: ComponentFixture<SetemployeeDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetemployeeDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetemployeeDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
