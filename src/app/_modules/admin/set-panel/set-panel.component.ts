import { Component, OnInit } from '@angular/core';
import { AlertService } from 'src/app/_service/alert.service';
import { JdService } from 'src/app/_service/jd.service';
import { ActivatedRoute } from '@angular/router';
import { ERFService } from 'src/app/_service/erf.service';
import {AuthenticationService} from 'src/app/_service/authentication.service';
declare var $: any;

@Component({
  selector: 'app-set-panel',
  templateUrl: './set-panel.component.html',
  styleUrls: ['./set-panel.component.css']
})
export class SetPanelComponent implements OnInit {

  setPanelData: any[];
  jdId: string;
  interviewType: any;
  addsetInternalForm: internalPanel[] = [];
  addsetExternalForm: externalPanel[] = [];
  formshowInternal: boolean = true;
  userDetailsBySapId: any;
  resumeId: string;
  round = 0;
  permissionData: any;
  PANEL_UPDATE: any;
  PANEL_ADD: any;
  // formshowExternal: boolean = false;
  constructor(private alertService: AlertService, private erfService: ERFService,
    private jdService: JdService, private route: ActivatedRoute,
    private authserv:AuthenticationService) { }

  ngOnInit() {
    this.setPanelData = [];
    this.route.paramMap.subscribe(params => {
      this.resumeId = params.get('resumeId');
      this.jdId = params.get('jdId');
      console.log(this.resumeId, this.jdId);
      this.getSetPanelData(this.jdId, this.resumeId);
      this.initialiseFormData()
    });
    this.setpermission();
  }
  initialiseFormData() {
    this.interviewType = ''
    this.addsetInternalForm = [{
      id: 0,
      employee: {
        id: 0
      },
      sapId: '',
      primary: false,
      username: '',
      isSubmited: false,
      isPrimarySelected: false,
    }];
    this.addsetExternalForm = [{
      id: 0,
      firstName: '',
      lastName: '',
      email: '',
      contact: '',
      primary: false,
      isSubmited: false,
      isPrimarySelected: false,
    }]
  }
  getSetPanelData(id, resumeid) {
    this.jdService.getsetPanel(id, resumeid).subscribe(res => {
      if (res) {
        this.setPanelData = res;
        const len = this.setPanelData.length - 1; 
        if(len >= 0){
          this.round = this.setPanelData[len].round;
          console.log(len, this.round);
        }
      }
    }, err => {
      this.alertService.error(err);
    });
  }
  oninterviewTypeChange(interviewTypes) {
    console.log(interviewTypes.value);
    if (interviewTypes.value === 'EXT_F2F' || interviewTypes.value === 'EXT_REMOTE') {
      this.formshowInternal = false;
      this.addsetExternalForm = [{
        id: 0,
        firstName: '',
        lastName: '',
        email: '',
        contact: '',
        primary: false,
        isSubmited: false,
        isPrimarySelected: false,
      }]
    } else {
      this.formshowInternal = true;
      this.addsetInternalForm = [{
        id: 0,
        employee: {
          id: 0
        },
        sapId: '',
        primary: false,
        username: '',
        isSubmited: false,
        isPrimarySelected: false,
      }]
    }
  }
  // get user
  getUserBySapID(id) {
    if (id !== '') {
      this.erfService.getSearchUserBySapId(id).subscribe(res => {
        this.userDetailsBySapId = res;
        // console.log(this.userDetailsBySapId);
      });
    }
  }
  // add empl id
  showuserName(user, index) {
    for (let i = 0; i < this.userDetailsBySapId.length; i++) {
      if (user === this.userDetailsBySapId[i].sapId) {
        this.addsetInternalForm[index].username = this.userDetailsBySapId[i].username;
        this.addsetInternalForm[index].employee.id = this.userDetailsBySapId[i].id;
      }
    }
  }
  addInternalRowForm() {
    const newObj = {
      id: this.addsetInternalForm.length,
      employee: {
        id: 0
      },
      sapId: '',
      primary: false,
      username: '',
      isSubmited: false,
      isPrimarySelected: false,
    }
    this.addsetInternalForm.push(newObj);
  }
  addExternalRowForm() {
    const newObj = {
      id: this.addsetExternalForm.length,
      firstName: '',
      lastName: '',
      email: '',
      contact: '',
      primary: false,
      isSubmited: false,
      isPrimarySelected: false,
    }
    this.addsetExternalForm.push(newObj);
  }

  addInternalSubmit() {
    this.addsetInternalForm.forEach(r => r.isSubmited = true)
    const primaryFlag: boolean = this.addsetInternalForm.some(r => r.primary === true)
    if (!primaryFlag) {
      this.addsetInternalForm.forEach(r => r.isPrimarySelected = true)
      this.alertService.success('Please, Set anyone as Primary!');
      return;
    }
    const newArr = this.addsetInternalForm;
    newArr.forEach(function (v) {
      delete v.id;
      delete v.username;
      delete v.sapId;
    });
    const newOBJ = {
      "jobDescription": {
        "id": Number(this.jdId)
      },
      "resume": {
        "resumeId": this.resumeId
      },
      "interviewType": this.interviewType,
      "round": this.round + 1,
      "internalPanel": newArr,
      "externalPanel": []
    };
    console.log(newOBJ);
    this.jdService.addSetPanel(newOBJ).subscribe(res => {
      if (res) {
        this.setPanelData = res;
        this.alertService.success('Added successfully.');
        this.getSetPanelData(this.jdId, this.resumeId);
        this.initialiseFormData()
        $("#addinterviewRound").modal("hide"); // toggle, show, hide
      }
    }, err => {
      this.alertService.error(err);
    });
  }
  deleteinternalFormRow(index) {
    this.addsetInternalForm = this.addsetInternalForm.filter(item => item.id !== index);
  }

  addExternalSubmit() {
    this.addsetExternalForm.forEach(r => r.isSubmited = true)
    const primaryFlag: boolean = this.addsetExternalForm.some(r => r.primary === true)
    if (!primaryFlag) {
      this.addsetExternalForm.forEach(r => r.isPrimarySelected = true)
      this.alertService.success('Please, Set anyone as Primary!');
      return;
    }
    const newArrEx = this.addsetExternalForm;
    newArrEx.forEach(function (v) { delete v.id });
    const newOBJ = {
      "jobDescription": {
        "id": this.jdId
      },
      "resume": {
        "resumeId": this.resumeId
      },
      "interviewType": this.interviewType,
      "round": this.round + 1,
      "internalPanel": [],
      "externalPanel": newArrEx
    };
    console.log(newOBJ);
    this.jdService.addSetPanel(newOBJ).subscribe(res => {
      if (res) {
        this.setPanelData = res;
        this.alertService.success('Added successfully.');
        this.getSetPanelData(this.jdId, this.resumeId);
        this.initialiseFormData()
        $("#addinterviewRound").modal("hide");
      }
    }, err => {
      this.alertService.error(err);
    });
  }
  deleteexternalFormRow(index) {
    this.addsetExternalForm = this.addsetExternalForm.filter(item => item.id !== index);
  }
  setpermission(){
    this.permissionData= this.authserv.receive();
    
    if(this.permissionData===undefined){
    this.authserv.setpermission2().subscribe((res)=>{
     this.permissionData =res;
     this.permissionData= this.authserv.send(this.permissionData);
   if( this.permissionData ){
   
   
    this.PANEL_ADD=this.permissionData['PANEL_ADD'],
    this.PANEL_UPDATE=this.permissionData.PANEL_UPDATE,
    
    console.log("if setpanel",this.permissionData);
  }})
}

  else{
  
    this.PANEL_ADD=this.permissionData['PANEL_ADD'],
    this.PANEL_UPDATE=this.permissionData.PANEL_UPDATE,
    
    console.log("else setpanel",this.permissionData);
  }
    PANEL_ADD: true
PANEL_UPDATE: true
PANEL_VIEW: true
  }
}
interface internalPanel {
  id: number;
  employee: {
    id: number
  };
  sapId: string;
  primary: boolean;
  username: string;
  isSubmited: boolean;
  isPrimarySelected: boolean;
}
interface externalPanel {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  contact: string;
  primary: boolean;
  isSubmited: boolean;
  isPrimarySelected: boolean;
}
// const newOBJ = {
//   "jobDescriptionResume":{
//     "id": this.jdId
//   },
//    "interviewType": this.interviewType,
//    "round": 0,
//    "internalPanel": ''
// };
// console.log(this.addsetInternalForm);
// for (let i = 0; i < this.addsetInternalForm.length; i++) {
//   newOBJ.internalPanel[i]['employee']['id'] = this.addsetInternalForm[i]['employee'];
//   newOBJ.internalPanel[i]['primary'] = this.addsetInternalForm[i].primary;
// }
// console.log(newOBJ);