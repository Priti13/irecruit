import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetPanelComponent } from './set-panel.component';

describe('SetPanelComponent', () => {
  let component: SetPanelComponent;
  let fixture: ComponentFixture<SetPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
