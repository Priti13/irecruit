import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/_service/authentication.service';
import { CurrentUser } from 'src/app/_model/user';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  currentUserValue: CurrentUser;
  name: string;

  constructor(private authService: AuthenticationService) {
    this.name = this.authService.currentUserValue.name;
    // console.log('Resopnse', this.name);
  }
  ngOnInit() { }
  logout() {
    this.authService.removeCurrentUser();
    this.authService.send(undefined);
  }
}
