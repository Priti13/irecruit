import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RasReportDetailComponent } from './ras-report-detail.component';

describe('RasReportDetailComponent', () => {
  let component: RasReportDetailComponent;
  let fixture: ComponentFixture<RasReportDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RasReportDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RasReportDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
