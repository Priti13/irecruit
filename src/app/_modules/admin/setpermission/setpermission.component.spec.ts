import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SetpermissionComponent } from './setpermission.component';

describe('SetpermissionComponent', () => {
  let component: SetpermissionComponent;
  let fixture: ComponentFixture<SetpermissionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SetpermissionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SetpermissionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
