import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AssignRecruiterService } from 'src/app/_service/assign-recruiter.service';
import { ActivatedRoute } from '@angular/router';
import { AlertService } from 'src/app/_service/alert.service';

import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { NgxSpinnerService } from 'ngx-spinner';


@Component({
  selector: 'app-setpermission',
  templateUrl: './setpermission.component.html',
  styleUrls: ['./setpermission.component.css']
})
export class SetpermissionComponent implements OnInit {

  @ViewChild('closebutton') closebutton;

  roleid: any;
  userId: any;
  setid: any;
  permission: any;
  status: any;

  empName: any;
  empId: any;
  editPermission: any;

  //Post value
  department: any[] = [];
  country: any[] = [];
  project: any[] = [];
  OfficeLocation: any[] = [];
  Entity: any[] = [];
  resourceAllocationId: any;
  postdata: any;
  permissionId: any;
  StatusPost: any ;
  //
  permissionDeparmentList: any[] = [];
  dropdowndepartmentSettings: IDropdownSettings = {
    singleSelection: false,
    idField: 'departmentId',
    textField: 'departmentName',
    selectAllText: 'SELECT ALL',
    unSelectAllText: 'UNSELECT ALL',
    itemsShowLimit: 3,
    allowSearchFilter: true
  };
  permissionCountryList: any[] = [];
  dropdowncountrySettings: IDropdownSettings = {
    singleSelection: false,
    idField: 'countryId',
    textField: 'name',
    selectAllText: 'SELECT ALL',
    unSelectAllText: 'UNSELECT ALL',
    itemsShowLimit: 3,
    allowSearchFilter: true
  };
  permissionProjectList: any[] = [];
  dropdownProjectSettings: IDropdownSettings = {
    singleSelection: false,
    idField: 'projectId',
    textField: 'projectName',
    selectAllText: 'SELECT ALL',
    unSelectAllText: 'UNSELECT ALL',
    itemsShowLimit: 3,
    allowSearchFilter: true
  };
  permissionOfficeList: any[] = [];
  dropdownOfficeLocationSettings: IDropdownSettings = {
    singleSelection: false,
    idField: 'id',
    textField: 'location',
    selectAllText: 'SELECT ALL',
    unSelectAllText: 'UNSELECT ALL',
    itemsShowLimit: 3,
    allowSearchFilter: true,
  };
  permissionEntityList: any[] = [];
  dropdownEntitySettings: IDropdownSettings = {
    singleSelection: false,
    idField: 'entityId',
    textField: 'entityName',
    selectAllText: 'SELECT ALL',
    unSelectAllText: 'UNSELECT ALL',
    itemsShowLimit: 3,
    allowSearchFilter: true
  };
  editperResourceAllocation: any[] = [];

  departmentId: any[] = [];
  countryId: any[] = [];
  officeId: any[] = [];
  EntityId: any[] = [];
  projectId: any[] = [];
  allSetPermissionData: any[] = [];
  selectedValue: string;

  constructor(private assignrecruiserv: AssignRecruiterService, private route: ActivatedRoute,
    private alert: AlertService, private spinner: NgxSpinnerService, ) { }

  ngOnInit() {
    this.selectedValue = 'Choose your Role';
    this.resourceAllocationId = 'Select Resource Allocation';
    this.route.paramMap.subscribe(params => {
      this.userId = params.get('id');
      this.getEmpName();
    });
    this.getrolesdropdown();
    this.getAllSetPermission(this.userId);
  }

  getEmpName() {
    this.empName = this.assignrecruiserv.setEmpName()[0];
    this.empId = this.assignrecruiserv.setEmpName()[1];
    console.log("Empname: " + this.assignrecruiserv.setEmpName()[0]);

    if (this.empName === undefined) {
      this.assignrecruiserv.createpermission().subscribe((res) => {
        this.empName = res[this.userId - 1].name;
        this.empId = res[this.userId - 1].id;
        // console.log("Shantam",res[this.userId-1],this.empName,this.empId)
      });

    }
  }

  getrolesdropdown() {
    return this.assignrecruiserv.getrolesdropdown().subscribe((res) => {
      this.roleid = res
    });
  }

  getAllSetPermission(userId) {
    this.spinner.show();
    this.assignrecruiserv.allsetPermission(userId).subscribe((res) => {
      if (res) {
        this.spinner.hide();
        this.allSetPermissionData = res;
      } else {
        this.spinner.hide();
        this.alert.error(res);
      }
    }, err => {
      this.spinner.hide();
      this.alert.error(err);
    });
  }

  onOptionsSelected(selectedRole) {
    console.log(selectedRole);
    this.setid = selectedRole.roleId;
  }

  setbutton() {
    if (this.setid === undefined) {
      this.alert.error("Please select role id ");
    }
    else {
      this.assignrecruiserv.setpermission(this.userId, this.setid).subscribe(res => {
        if (res) {
          this.selectedValue = 'Choose your Role';
          this.getAllSetPermission(this.userId);
          this.alert.success('Role set successfully.');
        } else {
          this.alert.error('Not Updated.');
        }
      }, err => {
        this.alert.error(err);
      });
    }
  }

  getstatus(per) {
    console.log("permission", this.status, per);
    this.status = per.active;
    this.permissionId = per.permissionId;
    this.editPermission = per.permissionConditions;
    console.log(this.permissionId, this.editPermission);
    if (this.editPermission) {
      this.spinner.show();
      this.assignrecruiserv.editpermission().subscribe((res) => {
        if (res) {
          this.spinner.hide();
          this.permissionDeparmentList = res['departments'];
          this.permissionCountryList = res['countries'];
          this.permissionProjectList = res['projects'];
          this.permissionOfficeList = res['officeLocations'];
          this.permissionEntityList = res['entities'];
          this.editperResourceAllocation = res['resourceAllocations'];
        }
      }, err => {
        this.spinner.hide();
        this.alert.error(err);
      });
    }

  }

  postPermission() {
    this.spinner.show();
    for (var i = 0; i <= this.department.length - 1; i++) {
      this.departmentId.push(this.department[i].departmentId);
    }
    for (var i = 0; i <= this.country.length - 1; i++) {

      this.countryId.push(this.country[i].countryId);
    }
    for (var i = 0; i <= this.OfficeLocation.length - 1; i++) {
      this.officeId.push(this.OfficeLocation[i].id);
    }
    for (var i = 0; i <= this.Entity.length - 1; i++) {
      this.EntityId.push(this.Entity[i].entityId);
    }
    for (var i = 0; i <= this.project.length - 1; i++) {
      this.projectId.push(this.project[i].projectId);
    }
    this.postdata = {
      "user": { "id": this.userId }, "condition": [{
        "permission": this.permissionId, "active": this.StatusPost, "country": this.countryId, "department": this.departmentId,
        "entity": this.EntityId, "project": this.projectId, "resourceAllocation": [this.resourceAllocationId]
      }]
    };
    if (!this.countryId || !this.EntityId ||
      !this.projectId || !this.departmentId || !this.resourceAllocationId) {
      this.alert.error("Please select all data ");
    } else {
      this.assignrecruiserv.postEditPermission(this.postdata).subscribe((res) => {
        if (res) {
          this.spinner.hide();
          this.closebutton.nativeElement.click();
          this.resourceAllocationId = 'Select Resource Allocation';
          this.getAllSetPermission(this.userId);
          this.alert.success('Updated successfully.');
          this.department = [];
          this.country = [];
          this.project = [];
          this.OfficeLocation = [];
          this.Entity = [];
          this.resourceAllocationId = '';
          // this.permissionDeparmentList = [];
          // this.permissionCountryList = [];
          // this.permissionProjectList = [];
          // this.permissionOfficeList = [];
          // this.permissionEntityList = [];
          // this.editperResourceAllocation = [];
        }
      }, err => {
        this.spinner.hide();
        this.alert.error(err);
      });

    }
    this.departmentId = [];
    this.countryId = [];
    this.officeId = [];
    this.EntityId = [];
    this.projectId = [];

  }
  ActiveValue(value) {
    console.log(typeof value);
    if (value === "In-Active") {
      this.StatusPost = false;
      console.log(this.StatusPost);
    }
    else {
      this.StatusPost = true;
      console.log(this.StatusPost);
    }

  }
  Goclick() {
    this.spinner.show();
    this.assignrecruiserv.setActiveInActive(this.userId, this.permissionId).subscribe((res) => {
      if (res) {
        this.spinner.hide();
        this.closebutton.nativeElement.click();
        this.alert.success('Role set successfully.');
        this.getAllSetPermission(this.userId);
      } else {
        this.alert.error('Not Updated.');
      }
    }, err => {
      this.spinner.hide();
      this.alert.error(err);
    });
  }

  public onitemSelect(item: any) {
  }

  public onitemSelectAll(items: any) {
  }

}
