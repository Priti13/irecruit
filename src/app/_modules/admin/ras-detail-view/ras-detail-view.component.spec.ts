import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RasDetailViewComponent } from './ras-detail-view.component';

describe('RasDetailViewComponent', () => {
  let component: RasDetailViewComponent;
  let fixture: ComponentFixture<RasDetailViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RasDetailViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RasDetailViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
