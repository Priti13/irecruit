import { Component, OnInit } from '@angular/core';
import { GenerateReport } from 'src/app/_service/generate-report.services';

@Component({
  selector: 'app-ras-detail-view',
  templateUrl: './ras-detail-view.component.html',
  styleUrls: ['./ras-detail-view.component.css']
})
export class RasDetailViewComponent implements OnInit {
  Viewdata: any;
  constructor(private generateReport: GenerateReport) {

  }

  ngOnInit() {
    this.getviewdata();
  }

  getviewdata() {
    this.Viewdata = this.generateReport.getData()
    if (this.Viewdata) {
      this.Viewdata = this.generateReport.getData();
    } else {
      this.generateReport.getReportData().subscribe((res) => {
        this.Viewdata = res[localStorage.getItem('id')]
      })
    }
  }

}
