import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/_model/user';
import { AuthenticationService } from 'src/app/_service/authentication.service';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/_service/alert.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { UserService } from 'src/app/_service/user.service';
import * as introJs from 'intro.js/intro.js';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  introJS = introJs();
  isLogin: boolean;
  loginForm: User;

  forgotState: { isEnterEmail: boolean; isVerifyOTP: boolean; isNewPassword: boolean; misMatch: boolean; };
  forgotForm: { forgotEmail: string; otp: string; newPassword: string; confirmPassword: string; };

  msg = 'Login Successfully!';
  errormsg = 'Username & Password is not correct';

  constructor(
    private authService: AuthenticationService, private userService: UserService,
    private router: Router, private alertService: AlertService, private spinner: NgxSpinnerService
  ) {
    this.introJS.setOptions({
      steps: [
        {
          element: '#step1',
          intro: 'Welcome to your new app!',
          position: 'top'
        },
        {
          element: '#step2',
          intro: "Enter Username",
          position: 'right'
        },
        {
          element: '#step3',
          intro: "Enter Password",
          position: 'right'
        },
        {
          element: '#step4',
          intro: 'Login your App',
          position: 'bottom'
        }
      ],
      showProgress: true
    });
   }
  ngOnInit() {
    // TODO: already logged-in, so return dashboard
    if (this.authService.currentUserValue && this.authService.currentUserValue.token &&
      this.authService.currentUserValue.permissions && this.authService.currentUserValue.permissions.length >= 1) {
      this.router.navigateByUrl("/dashboard")
    }
    this.initMethod();
  }
  initMethod() {
    this.isLogin = true;
    this.loginForm = new User();
    this.forgotState = { isEnterEmail: false, isVerifyOTP: false, isNewPassword: false, misMatch: false };
    this.forgotForm = { forgotEmail: '', otp: '', newPassword: '', confirmPassword: '' };
  }
  login() {
    this.spinner.show();
    this.authService.removeCurrentUser();
   
    this.authService.login(this.loginForm).subscribe(_ => {
      this.spinner.hide();
      this.authService.permission().subscribe(res => {
        this.alertService.success(this.msg, true);
        this.router.navigate(['/dashboard']); 
       // console.log("routing con"+this.router.navigate(['/dashboard']));
      }, error => {
        console.log(error)
        this.spinner.hide();
        this.alertService.error(this.errormsg, true);
      });
    
      
    });
  }
  oldLogin() {
    this.spinner.show();
    // TODO: Set Initial State for currentUser
    this.authService.removeCurrentUser();
    this.authService.login(this.loginForm).subscribe(resp => {
      this.spinner.hide();
     // this.authService.permission().subscribe(res => {
     // this.authService.permission2();
      // this.spinner.hide();
     // var permission=this.authService.setpermission();
     //if(permission){
      this.alertService.success(this.msg, true);
      console.log('logout');
      
      this.router.navigate(['/dashboard']);
    // }
    }, error => {
      this.spinner.hide();
      this.alertService.error(this.msg);
    });
  }
  enableLoginForm() {
    this.initMethod()
  }
  enableForgotForm() {
    this.isLogin = false;
    this.forgotState.isEnterEmail = true;
    this.forgotState.misMatch = false;
    this.forgotForm.forgotEmail = this.loginForm.username;
  }
  sendForgotEmail() {
    if (this.forgotForm.forgotEmail === '') {
      this.forgotState.misMatch = true;
      return;
    }
    this.userService.forgetPassword(this.forgotForm.forgotEmail).subscribe(
      res => {
        console.log(this.forgotForm, res)
        if (res) {
          this.forgotState.misMatch = false;
          this.forgotState.isEnterEmail = false;
          this.forgotState.isVerifyOTP = true;
          this.alertService.success("Write the otp which is sended to given Mail ID");
        } else
          this.alertService.error("Unable to send OTP to given Mail ID");
      }, err => {
        this.alertService.error(err);
      });
  }
  sendForgotOTP() {
    if (this.forgotForm.otp === '') {
      this.forgotState.misMatch = true;
      return;
    }
    this.userService.forgetValidateOtp(this.forgotForm.forgotEmail, this.forgotForm.otp).subscribe(
      res => {
        console.log(this.forgotForm, res)
        if (res['status']) {
          this.forgotState.misMatch = false;
          this.forgotState.isVerifyOTP = false;
          this.forgotState.isNewPassword = true;
          this.alertService.success(res['message']);
        } else
          this.alertService.error(res['message']);
      }, err => {
        this.alertService.error(err);
      });
  }
  forgotPassword(fp) {
    if (fp.value.newPassword !== fp.value.confirmPassword) {
      this.forgotState.misMatch = true;
      return;
    }
    this.userService.forgetConfirmPassword(this.forgotForm.forgotEmail, this.forgotForm.newPassword).subscribe(
      res => {
        console.log(fp.value, this.forgotForm, res)
        if (res['status']) {
          fp.resetForm();
          /* Start From Initial */
          this.initMethod();
          this.alertService.success(res['message'] + ", Pease Login Again!");
        } else
          this.alertService.error(res['message']);
      }, err => {
        this.alertService.error(err);
      });
  }
}
