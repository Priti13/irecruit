import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewErfComponent } from './view-erf.component';

describe('ViewErfComponent', () => {
  let component: ViewErfComponent;
  let fixture: ComponentFixture<ViewErfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewErfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewErfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
