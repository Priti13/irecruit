import { Component, OnInit } from '@angular/core';
import { ERFService } from 'src/app/_service/erf.service';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/_service/alert.service';
import{AuthenticationService} from 'src/app/_service/authentication.service';

@Component({
  selector: 'app-view-erf',
  templateUrl: './view-erf.component.html',
  styleUrls: ['./view-erf.component.css']
})
export class ViewErfComponent implements OnInit {
  selectedType = '';
  viewErfData: any = [];
  selected: boolean;
  remarkModel: any = [];
  project_initiate:any;
  permissionData:any;
  constructor(private erfService: ERFService, private router: Router, private alertService: AlertService,private authserv:AuthenticationService) { }
  ngOnInit() {
    this.viewErfDetail();
    this.permissionstatus();
   
  }
  viewErfDetail() {
    this.erfService.viewErf().subscribe(res => {
      if (res) {
        this.viewErfData = res;
        this.viewErfData = this.viewErfData.map(r => {
          r['selected'] = (r['approved'] == 'ACTIVE') ? true : false
          return r;
        })
      }
    });
  }
  onStatusChange(viewdata) {
    this.erfService.getApproveERF(viewdata.erfId).subscribe(res => {
      if (res['status']) {
        this.viewErfDetail();
        this.alertService.success(res['message']);
        console.log("project status",res);
      } else {
        viewdata.selected = !viewdata.selected;
        this.alertService.success(res['message']);
      }
    });
  }
  onRemarkModel(remark: any[]) {
    this.remarkModel = remark
  }
  onChange(event) {
    this.selectedType = event.target.value;
  }
  navigateToNext(item) {
    if (item['approved'] === 'ACTIVE')
      this.router.navigate(['/detail-project/', item.erfId])
  }
  permissionstatus(){
    this.permissionData= this.authserv.receive();
    if(this.permissionData===undefined){
      this.authserv.setpermission2().subscribe((res)=>{
     this.permissionData =res;
     this.permissionData= this.authserv.send(this.permissionData);
        //console.log("sideapi",this.permissionData);
        if(this.permissionData){
    console.log("viewresume",this.permissionData);
    this.project_initiate=this.permissionData['PROJECT_INITIATE'],
    
    
    console.log("view erf",this.permissionData);
  }})}
  else{
  
    this.project_initiate=this.permissionData['PROJECT_INITIATE'],
    
    console.log("view erf",this.permissionData);
  }
   
  
/*return this.authserv.permission2().subscribe((res)=>{this.project_initiate=res.PROJECT_INITIATE; 
  console.log("Shantam"+this.project_initiate);
  });*/
}
}
