import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AdminModule } from '../app/_modules/admin/admin.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthenticationService } from './_service/authentication.service';
import { JwtInterceptor } from './_helpers/jwt.interceptor';
import { TagInputModule } from 'ngx-chips';
import { ErrorInterceptor } from './_helpers/error.interceptor';
import { SafePipe, CalendarPipe } from './Pipe/safe.pipe';


@NgModule({
  declarations: [
    AppComponent,
    CalendarPipe,
    SafePipe,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AdminModule,
    HttpClientModule,
    TagInputModule,
    BrowserAnimationsModule,

  ],
  providers: [
    AuthenticationService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    CalendarPipe,
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
