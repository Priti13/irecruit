import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

import { AuthenticationService } from '../_service/authentication.service';
import { Router } from '@angular/router';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthenticationService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        const currentUser = this.authenticationService.currentUserValue;
        if (currentUser && currentUser['type'] === 'bearer' && !currentUser.isExpire) {
            request = request.clone({
                setHeaders: {
                    Authorization: `${currentUser['type']} ${currentUser['token']}`
                }
            });
        } else {
            request = request.clone({
                setHeaders: {
                    Authorization: 'Basic ' + btoa('irecruitClient:wmbDR19DvstC8')
                }
            });
        }
        return next.handle(request);
    }
}
