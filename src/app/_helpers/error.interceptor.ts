import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { AlertService } from '../_service/alert.service';
import { AuthenticationService } from '../_service/authentication.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    constructor(private alertService: AlertService, private authService: AuthenticationService,
        private spinner: NgxSpinnerService) { }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError(err => {
            this.spinner.hide();
            if ([400, 403].indexOf(err.status) !== -1) {
                this.alertService.error(err.error['error_description'])
            } else if (err.status === 401 && !this.authService.currentUserValue.isExpire) {
                this.alertService.error("Session Expired!")
                this.authService.currentUserValue.isExpire = true;
                return this.authService.refreshLogin()
            } else if (err.status === 500) {
                this.alertService.error("Server Side Error!")
            }
            // console.error(err.status, err.error, err.statusText)
            let error = err.error || err.statusText;
            return throwError(error);
        }))
    }
}
