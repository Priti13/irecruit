import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate, Router } from '@angular/router';
import { AuthenticationService } from '../_service/authentication.service';



@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router, private authService: AuthenticationService) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    // tslint:disable-next-line: max-line-length
    if (this.authService.currentUserValue && this.authService.currentUserValue.token && this.authService.currentUserValue.permissions && this.authService.currentUserValue.permissions) {
      // logged in so return true
      return true;
    } else {
      // not logged in so redirect to login page with the return url
    this.router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
    // this.router.navigate(['/login']);
    return false;

    }
  }
}
