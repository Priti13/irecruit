import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';

const url = environment.url;

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  constructor(
    private http: HttpClient,
  ) { }

  // get all country list
  getCountryList() {
    return this.http.get(url + 'country/find-all')
      .pipe(catchError(this.errorHandler));
  }
  //
  getCountryByName(name) {
    return this.http.get(url + `country/find-by-nicename/${name}`)
      .pipe(catchError(this.errorHandler));
  }
  //
  getStateByName(name) {
    return this.http.get(url + `state/find-by-nicename/${name}`)
      .pipe(catchError(this.errorHandler));
  }
  // get all state list
  getStateList(countryId: number) {
    return this.http.get(url + `state/find-by-country/${countryId}`)
      .pipe(catchError(this.errorHandler));
  }
  // get all wage list
  getWageList() {
    return this.http.get(url + 'wagetype/get-wagetype')
      .pipe(catchError(this.errorHandler));
  }
  // get all resource-allocation list
  getResourceAllocation() {
    return this.http.get(url + 'resource-allocation')
      .pipe(catchError(this.errorHandler));
  }
  errorHandler(respError: HttpErrorResponse | any) {
    if (respError.error instanceof ErrorEvent) {
      console.error('Client Side Error: ' + respError);
    }
    // else {
    //   console.error('Server Side Error: ' + respError);
    // }
    return throwError(respError || 'Server Downgrade Error');
  }
}
