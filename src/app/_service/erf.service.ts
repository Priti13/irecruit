import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { GernerateErf } from '../_model/user';

const url = environment.url;

@Injectable({
    providedIn: 'root'
})
export class ERFService {

    constructor(private http: HttpClient) { }
    // save Remark Api
    setRemark(remarkForm: any, erfId: number) {
        return this.http.post(url + `irecruit-api/erf/save-remark/${erfId}`, remarkForm)
            .pipe(catchError(this.errorHandler));
    }
    // Approve Erf Api
    getApproveERF(erfId) {
        return this.http.get(url + `irecruit-api/erf/approve-project/${erfId}`)
            .pipe(catchError(this.errorHandler));
    }
    // View Erf Api Data
    getAllDetailsForAdd() {
        return this.http.get(url + 'irecruit-api/metadata/get-for-erf-generation')
            .pipe(catchError(this.errorHandler));
    }
    getBuHead(): Observable<any> {
        return this.http.get(url + 'irecruit-api/user/get-buhead').pipe(catchError(this.errorHandler));
    }
    getSaleSpoc(): Observable<any> {
        return this.http.get(url + 'irecruit-api/user/get-salespoc').pipe(catchError(this.errorHandler));
    }
    // Save ERF form Post Api Data
    saveErf(result): Observable<any> {
        return this.http.post<any>(environment.url + 'irecruit-api/erf/save-project', result, {})
            .pipe(catchError(this.errorHandler));
    }
    editProject(editProForm, erfId) {
        return this.http.put<any>(environment.url + `irecruit-api/erf/edit-project/${erfId}`, editProForm)
            .pipe(catchError(this.errorHandler));
    }

    // View Erf Api Data
    viewErf() {
        return this.http.get(url + 'irecruit-api/erf/view-erf')
            .pipe(catchError(this.errorHandler));
    }
    // get erf breif detail
    getErfDetail(id) {
        return this.http.get(environment.url + `irecruit-api/erf/view-erf/${id}`)
            .pipe(catchError(this.errorHandler));
    }
    // get getSearchUserBySapId breif detail
    getSearchUserBySapId(sapId: number) {
        return this.http.get(environment.url + `irecruit-api/user/search-by-sapId/${sapId}`)
            .pipe(catchError(this.errorHandler));
    }
    // get erf breif detail
    getSearchUserByUsername(username) {
        return this.http.get(environment.url + `irecruit-api/user/search-by-username/${username}`)
            .pipe(catchError(this.errorHandler));
    }


    // View track lead Api Data
    trackLeadDetails(erfId) {
        return this.http.get(url + `irecruit-api/tracklead/get-trackleads/${erfId}`)
            .pipe(catchError(this.errorHandler));
    }
    // add new track lead form data
    addTrack(form): Observable<any> {
        return this.http.post<any>(environment.url + 'irecruit-api/tracklead/assign-tracklead', form, {})
            .pipe(catchError(this.errorHandler));
    }

    // Job description start here
    jobDescriptionDetails(erfId) {
        return this.http.get(url + `irecruit-api/jd/get-jd-by-erf/${erfId}`)
            .pipe(catchError(this.errorHandler));
    }
    getGenderEducationEthnicity() {
        return this.http.get(url + `metadata/get-data-for-jobdescription`)
            .pipe(catchError(this.errorHandler));
    }
    getAllAdrress(erfId) {
        return this.http.get(url + `irecruit-api/erf/get-location-by-erf/${erfId}`)
            .pipe(catchError(this.errorHandler));
    }
    // add JD form data
    addJobDes(form): Observable<any> {
        return this.http.post<any>(environment.url + 'irecruit-api/jd/add-jd', form, {})
            .pipe(catchError(this.errorHandler));
    }
    addJobDesNew(form): Observable<any> {
        return this.http.post<any>(environment.url + 'irecruit-api/jd/add-job-description', form, {})
            .pipe(catchError(this.errorHandler));
    }

    errorHandler(respError: HttpErrorResponse | any) {
        if (respError.error instanceof ErrorEvent) {
            console.error('Client Side Error: ' + respError);
        }
        // else {
        //   console.error('Server Side Error: ' + respError);
        // }
        return throwError(respError || 'Server Downgrade Error');
    }
}
