import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { catchError } from 'rxjs/operators';
import { throwError, Observable } from 'rxjs';
import { AlertService } from './alert.service';

const url = environment.url;

@Injectable({
  providedIn: 'root'
})
export class JdService {

  constructor(private http: HttpClient, public alertService: AlertService) { }

  // Get JD Details
  getJDDetails(): Observable<any> {
    return this.http.get(url + `irecruit-api/jd/get-jd`)
  }
  // Get Resumes Attached to a JD
  getResumesAttached(jdId: string): Observable<any> {
    return this.http.get(url + `irecruit-api/jd/get-attached-resumes/${jdId}`)
  }
  // Get JD & Resumes Attached to a JD ID
  getJDResumeDetail(jdId: string, resumeId: string): Observable<any> {
    return this.http.get(url + `irecruit-api/jd/getJDAndResume/${jdId}/${resumeId}`)
  }
  // API for set Interview Panel
  // Get set panel
  getsetPanel(jdId: string, resumeid): Observable<any> {
    return this.http.get(url + `irecruit-api/interview/get-interview-details/${jdId}/${resumeid}`)
  }
  addSetPanel(iPanel): Observable<any> {
    return this.http.post(url + `irecruit-api/interview/set-interview`, iPanel)
  }
  // get jd by jd Id
  // irecruit-api/jd/get-jd/{jdId}
  getSingleJd(jdId: string): Observable<any> {
    return this.http.get(url + `irecruit-api/jd/get-jd/${jdId}`);
  }
  // approve offer
  getApproveOffer(joiningId: string): Observable<any> {
    return this.http.post(url + `irecruit-api/offer/approve-joining-data/${joiningId}`, {});
  }
  getRejectOffer(joiningId: string, reason): Observable<any> {
    let formData: FormData = new FormData();
    formData.set('reason', reason)
    return this.http.post(url + `irecruit-api/offer/reject-joining-data/${joiningId}`, formData);
  }
}
