import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { AlertService } from './alert.service';

const url = environment.url;

@Injectable({
  providedIn: 'root'
})
export class GenerateReport {
  indexdata: any;

  constructor(private http: HttpClient, public alertService: AlertService) { }

  //Generate Report Tab
  getReportData(): Observable<any> {
    return this.http.get(url + `irecruit-api/report/consolidation-report`);
  }

  filterReport(result): Observable<any> {
    return this.http.post(url + 'irecruit-api/report/filter-report', result);
  }

  downloadReport(result): Observable<any> {
    return this.http.post(url + 'irecruit-api/report/download-report', result, { responseType: 'text' });
  }
  //Transfering data from one component to other Start
  setData(value) {
    this.indexdata = value;
  }

  getData() {
    return this.indexdata;
  }

  //Transfering data from one component to other End

}
