import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { environment } from '../../environments/environment';

const url = environment.url;

@Injectable({
  providedIn: 'root'
})
export class AssignRecruiterService {
  empName: any;
  empId: any;

  constructor(private http: HttpClient) { }
  // get all job details
  jobDescriptionDetails(): Observable<any> {
    return this.http.get(url + `irecruit-api/jd/get-jd`);
  }
  // start details api here
  // get all recruiter Api Data
  getassignedRecruiter(): Observable<any> {
    return this.http.get(url + 'irecruit-api/jd/get-recruiters');
  }
  // get job description by id Api Data
  jobDesById(jdId): Observable<any> {
    return this.http.get(url + `irecruit-api/jd/get-jd/${jdId}`);
  }
  assignedRecruitersByJd(jdId): Observable<any> {
    return this.http.get(url + `irecruit-api/jd/get-assigned-recruiters/${jdId}`);
  }
  // get job description by id Api Data
  assignRecruiter(form): Observable<any> {
    return this.http.post(url + `irecruit-api/jd/assign-recruiter`, form);
  }
  // Set-Calendar
  setCalendar(interviewId, interviewDateTime, timeZoneId): Observable<any> {
    let formData: FormData = new FormData();
    formData.set('interviewDateTime', interviewDateTime);
    formData.set('timeZoneId', timeZoneId);
    return this.http.post(url + `irecruit-api/interview/set-calendar/${interviewId}`, formData);
  }
  // set time zone for calendar
  getTimeZone(): Observable<any> {
    return this.http.get(url + `timezone/get-timezone`);
  }
  //Shantam
  createpermission(): Observable<any> {
    return this.http.get(url + "irecruit-api/app-permission/employee-details");
  };
  allsetPermission(userId): Observable<any> {
    return this.http.get(url + `irecruit-api/app-permission/get-user-role-permission/${userId}`);
  }
  getrolesdropdown(): Observable<any> {
    return this.http.get(url + "get-roles");
  }
  setpermission(userId, roleId): Observable<any> {
    return this.http.post(url + `irecruit-api/app-permission/assign-role/${userId}/${roleId}`, {});
  }

  setActiveInActive(userId, permissionId): Observable<any> {
    return this.http.post(url + `irecruit-api/app-permission/add-permission/${userId}/${permissionId}`, {});
  }

  getEmpName(name, id) {
    this.empName = name;
    this.empId = id;
  }
  setEmpName() {
    this.empName;
    this.empId;
    return [this.empName, this.empId]
  }
  editpermission() : Observable<any> {
    return this.http.get(url + `irecruit-api/metadata/get-for-erf-generation`);
  }
  postEditPermission(obj): Observable<any>  {
    return this.http.post(url + `irecruit-api/app-permission/set-permission-condition`, obj);
  }

  getUserDetail(userId): Observable<any>  {
    return this.http.get(url + `irecruit-api/user/user-details/${userId}`);
  }
  updateUserProfile(userId:any,Body:any): Observable<any>  {
   
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });

    return this.http.put(url + `irecruit-api/user/update-user-profile/${userId}`, Body, { headers: headers });
}
postEmpDetail(jdId,resumeId): Observable<any>  {
  const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
return this.http.post(url + `irecruit-api/user/add-new-candidate/${jdId}/${resumeId}`,{ headers: headers },{});
}

getOfferCandidatelist(): Observable<any>  {
  return this.http.get(url +`irecruit-api/user/candidate-details`);
}
}