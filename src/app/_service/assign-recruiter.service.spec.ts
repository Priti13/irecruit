import { TestBed } from '@angular/core/testing';

import { AssignRecruiterService } from './assign-recruiter.service';

describe('AssignRecruiterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AssignRecruiterService = TestBed.get(AssignRecruiterService);
    expect(service).toBeTruthy();
  });
});
