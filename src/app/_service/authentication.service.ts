import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError, BehaviorSubject } from 'rxjs';
import { map, catchError, filter } from 'rxjs/operators';
import { User, CurrentUser, RefreshUser } from '../_model/user';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { AlertService } from './alert.service';


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private headers: HttpHeaders = new HttpHeaders();
  private currentUserSubject: BehaviorSubject<CurrentUser>;
  public currentUser: Observable<CurrentUser>;
  //permission by shantam
  CALENDAR_RESCHEDULE: any;
CALENDAR_SET: any;
CALENDAR_VIEW: any;
CANDIDATE_ASSIGN_TO_JD: any;
CANDIDATE_UPLOAD: any;
EMPLOYEE_PERMISSION: any;
ERF_ACTION: any;
ERF_GENERATE: any;
ERF_VIEW: any;
FEEDBACK_ADD: any;
FEEDBACK_VIEW: any;
FEEDBACK_VIEW_JD: any;
FEEDBACK_VIEW_PANEL: any;
FEEDBACK_VIEW_RESUME: any;
GENERATE_REPORT: any;
JOBDESCRIPTION_ADD: any;
JOBDESCRIPTION_DELETE: any;
JOBDESCRIPTION_UPDATE: any;
JOBDESCRIPTION_VIEW: any;
JOB_VIEW: any;
JOININGDATA_FEEDBACK_VIEW: any;
JOININGDATA_JD_VIEW: any;
JOININGDATA_UPDATE: any;
JOININGDATA_VIEW: any;
OFFER_CONTROL: any;
OFFER_RELEASE: any;
OFFER_VIEW: any;
PANEL_ADD: any;
PANEL_UPDATE: any;
PANEL_VIEW: any;
PROJECT_GENERATE: any;
PROJECT_INITIATE: any;
PROJECT_STATUS: any;
PROJECT_UPDATE: any;
PROJECT_VIEW: any;
RECRUITER_ASSIGN: any;
RECRUITER_ASSIGN_TO_JD: any;
RECRUITER_VIEW: any;
RESUME_VIEW: any;
TRACKLEAD_ADD: any;
TRACKLEAD_DELETE: any;
TRACKLEAD_UPDATE: any;
TRACKLEAD_VIEW: any;
permissionData:any;

  constructor(private http: HttpClient, private router: Router, private alertService: AlertService) {
    this.currentUserSubject = new BehaviorSubject<CurrentUser>
      (JSON.parse(localStorage.getItem('currentUser')));
    // this.currentUser = this.currentUserSubject.asObservable();
  }
  public get currentUserValue(): CurrentUser {
    return this.currentUserSubject.value;
  }
  public setCurrentUser(currentUser: CurrentUser): boolean {
    localStorage.setItem('currentUser', JSON.stringify(currentUser));
    this.currentUserSubject.next(currentUser);
    return true;
  }
  public removeCurrentUser(): boolean {
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(JSON.parse(localStorage.getItem('currentUser')));
    this.router.navigate(['/'])
    return true;
  }
  // login(obj: User): Observable<any> {
  //   const formData: FormData = new FormData();
  //   for (const key in obj) {
  //     if (obj.hasOwnProperty(key)) {
  //       formData.append(key, obj[key]);
  //     }
  //   }
  //   return this.http.post<any>(environment.url + 'oauth/token', formData).pipe(
  //     map(userData => {
  //       let currentUser: CurrentUser = new CurrentUser();
  //       currentUser.type = userData['token_type'];
  //       currentUser.token = userData['access_token'];
  //       currentUser.refresh = userData['refresh_token'];

  //       this.setCurrentUser(currentUser);
  //       return true;
  //     }),
  //     filter(userData => userData === true));
  // }
  login(obj: User): Observable<any> {
    const formData: FormData = new FormData();
    for (const key in obj) {
      if (obj.hasOwnProperty(key)) {
        formData.append(key, obj[key]);
      }
    }
    return this.http.post<any>(environment.url + 'oauth/token', formData, { headers: this.headers }).pipe(
      map(userData => {
        let currentUser: CurrentUser = new CurrentUser();
        currentUser.type = userData['token_type'];
        currentUser.token = userData['access_token'];
        currentUser.refresh = userData['refresh_token'];
        currentUser.psw = obj.password;
        currentUser.isExpire = false;

        this.setCurrentUser(currentUser);
        return true;
      }),
      filter(userData => userData === true)
    );
  }
  permission(): Observable<any> {
    return this.http.get<any>(environment.url + 'irecruit-api/user/get-user-permissons').pipe(
      map(userData => {
        let currentUser: CurrentUser = this.currentUserValue;
        currentUser.name = userData['userData']['name'];
        currentUser.email = userData['userData']['email'];
        currentUser.designation = userData['userData']['designation'];
        currentUser.department = userData['userData']['department'];
        currentUser.sapId = userData['userData']['sapId'];
        currentUser.id = userData['userData']['id'];
        currentUser.permissions = userData['permisions'];

        this.setCurrentUser(currentUser);
        return userData;
      }));
  }
  refreshLogin(): Observable<any> {
    let refreshUser: RefreshUser = new RefreshUser(this.currentUserValue.name, this.currentUserValue.psw, this.currentUserValue.refresh);
    const formData: FormData = new FormData();
    for (const key in refreshUser) {
      if (refreshUser.hasOwnProperty(key)) {
        formData.append(key, refreshUser[key]);
      }
    }
    return this.http.post<any>(environment.url + 'oauth/token', formData).pipe(
      map(userData => {
        let currentUser: CurrentUser = this.currentUserValue;
        currentUser.token = userData['access_token'];
        currentUser.refresh = userData['refresh_token'];
        currentUser.isExpire = false;

        this.setCurrentUser(currentUser);
        this.alertService.success("Token ReStored | refresh the page...")
        return true;
      }),
      filter(userData => userData === true));
  }
  permission2(){
  //  console.log("permsiiiii"+this.permissionData)
     return this.http.get<any>(environment.url +`irecruit-api/user/set-dynamic-permission`).subscribe((res)=>{
      this.permissionData =res,
      console.log("permissss",this.permissionData);
     
    // this.setpermission();
    
    });
    /*.subscribe(res=>{
      this.permissionData=res;
      console.log("permsiiiii"+this.permissionData)
     this.CALENDAR_RESCHEDULE= res.CALENDAR_RESCHEDULE
this.CALENDAR_SET=res.CALENDAR_SET;
this.CALENDAR_VIEW=res.CALENDAR_VIEW;
this.CANDIDATE_ASSIGN_TO_JD=res.CANDIDATE_ASSIGN_TO_JD;
this.CANDIDATE_UPLOAD=res.CANDIDATE_UPLOAD;
this.EMPLOYEE_PERMISSION=res.EMPLOYEE_PERMISSION;
this.ERF_ACTION=res.ERF_ACTION;
this.ERF_GENERATE=res.ERF_GENERATE;
this.ERF_VIEW=res.ERF_VIEW;
this.FEEDBACK_ADD=res.FEEDBACK_ADD;
this.FEEDBACK_VIEW=res.FEEDBACK_VIEW;
this.FEEDBACK_VIEW_JD=res.FEEDBACK_VIEW_JD;
this.FEEDBACK_VIEW_PANEL=res.FEEDBACK_VIEW_PANEL;
this.FEEDBACK_VIEW_RESUME=res.FEEDBACK_VIEW_RESUME;
this.GENERATE_REPORT=res.GENERATE_REPORT;
this.JOBDESCRIPTION_ADD=res.JOBDESCRIPTION_ADD;
this.JOBDESCRIPTION_DELETE=res.JOBDESCRIPTION_DELETE;
this.JOBDESCRIPTION_UPDATE=res.JOBDESCRIPTION_UPDATE;
this.JOBDESCRIPTION_VIEW=res.JOBDESCRIPTION_VIEW;
this.JOB_VIEW=res.JOB_VIEW;
this.JOININGDATA_FEEDBACK_VIEW=res.JOININGDATA_FEEDBACK_VIEW;
this.JOININGDATA_JD_VIEW=res.JOININGDATA_JD_VIEW;
this.JOININGDATA_UPDATE=res.JOININGDATA_UPDATE;
this.JOININGDATA_VIEW=res.JOININGDATA_VIEW;
this.OFFER_CONTROL=res.OFFER_CONTROL;
this.OFFER_RELEASE=res.OFFER_RELEASE;
this.OFFER_VIEW=res.OFFER_VIEW;
this.PANEL_ADD=res.PANEL_ADD;
this.PANEL_UPDATE=res.PANEL_UPDATE;
this.PANEL_VIEW=res.PANEL_VIEW;
this.PROJECT_GENERATE=res.PROJECT_GENERATE;
this.PROJECT_INITIATE=res.PROJECT_INITIATE;
this.PROJECT_STATUS=res.PROJECT_STATUS;
this.PROJECT_UPDATE=res.PROJECT_UPDATE;
this.PROJECT_VIEW=res.PROJECT_VIEW;
this.RECRUITER_ASSIGN=res.RECRUITER_ASSIGN;
this.RECRUITER_ASSIGN_TO_JD=res.RECRUITER_ASSIGN_TO_JD;
this.RECRUITER_VIEW=res.RECRUITER_VIEW;
this.RESUME_VIEW=res.RESUME_VIEW;
this.TRACKLEAD_ADD=res.TRACKLEAD_ADD;
this.TRACKLEAD_DELETE=res.TRACKLEAD_DELETE;
this.TRACKLEAD_UPDATE=res.TRACKLEAD_UPDATE;
this.TRACKLEAD_VIEW=res.TRACKLEAD_VIEW;

    });*/
  }// Shantam  http://localhost:8090/irecruit-api/user/set-dynamic-permission
  setpermission(){
     console.log("set"+JSON.stringify(this.permissionData));
     return this.permissionData;
  }
  setpermission2(){
    if(this.permissionData===undefined){
     // this.permission2();
      console.log("globally permission hit");
    }
   return this.http.get<any>(environment.url +`irecruit-api/user/set-dynamic-permission`);
  }
  send(permissionData){
    this.permissionData=permissionData;
    console.log("newcreated",permissionData);
    return permissionData;
  }
  receive(){
    return this.permissionData;
  }
 
}
