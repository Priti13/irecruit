import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { AuthenticationService } from './authentication.service';
import { environment } from '../../environments/environment';

const url = environment.url;

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient,
    private authService: AuthenticationService
  ) { }

  getUserProfile(): Observable<any> {
    return this.http.get(url + 'irecruit-api/user/get-user');
  }
  getUserProfilePhoto(pic): Observable<any> {
    let formData: FormData = new FormData();
    // formData.set('countryCode', code)
    formData.set('file', pic)
    return this.http.post(url + 'irecruit-api/user/profile-image', formData);
    // return this.http.get(url + 'irecruit-api/user/profile-image');
  }
  sendOTP(code: string, mobileNo: string): Observable<any> {
    let formData: FormData = new FormData();
    formData.set('countryCode', code)
    formData.set('mobileNumber', mobileNo)
    return this.http.post(url + 'OTP/send', formData);
  }
  verifyOTP(otpnum: any, mobileNo: string): Observable<any> {
    let formData: FormData = new FormData();
    formData.set('otpnum', otpnum)
    formData.set('mobileNumber', mobileNo)
    return this.http.post(url + 'OTP/verify', formData);
  }
  changePassword(currentPassword: string, changePassword: string): Observable<any> {
    let formData: FormData = new FormData();
    formData.set('currentPassword', currentPassword)
    formData.set('changePassword', changePassword)
    return this.http.post(url + 'irecruit-api/password/change-password', formData);
  }
  forgetPassword(email: string): Observable<any> {
    let formData: FormData = new FormData();
    formData.set('email', email)
    return this.http.post(url + 'password/forget-password', formData);
  }
  forgetValidateOtp(email: string, otpnum: string): Observable<any> {
    let formData: FormData = new FormData();
    formData.set('email', email)
    formData.set('otpnum', otpnum)
    return this.http.post(url + 'password/validate-Otp', formData);
  }
  forgetConfirmPassword(email: string, newPassword: string): Observable<any> {
    let formData: FormData = new FormData();
    formData.set('email', email)
    formData.set('newPassword', newPassword)
    return this.http.post(url + 'password/confirm-password', formData);
  }
  // to set theme
  updateThemecolor(theme: string): Observable<any> {
    let formData: FormData = new FormData();
    formData.set('theme', theme)
    return this.http.post(url + 'irecruit-api/user/theme', formData);
  }
}
