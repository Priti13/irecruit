import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';

const url = environment.url;

@Injectable({
  providedIn: 'root'
})
export class UploadService {

  constructor(
    private http: HttpClient
  ) { }

  // upload resume
  uploadResume(uploadResume): Observable<any> {
    const endpoint = 'your-destination-url';
    const formData: FormData = new FormData();
    formData.append('firstName', uploadResume.firstName);
    formData.append('lastName', uploadResume.lastName);
    formData.append('email', uploadResume.email);
    formData.append('phone', uploadResume.phone);
    formData.append('domain', uploadResume.domain);
    formData.append('countryId', uploadResume.countryId);
    formData.append('address', uploadResume.address);
    formData.append('city', uploadResume.city);
    formData.append('stateId', uploadResume.stateId);
    formData.append('zip', uploadResume.zip);
    formData.append('file', uploadResume.file);
    // formData.append('fileKey', fileToUpload, fileToUpload.name);
    return this.http.post<any>(url + 'irecruit-api/resume/save-resume', formData)
      .pipe(catchError(this.errorHandler));
  }

  // search dropdown by email
  getResumesAttachedByEmail(email): Observable<any> {
    return this.http.get(url + `irecruit-api/resume/search-resume-by-email/${email}`)
  }
  // search dropdown by firstName
  getResumesAttachedByFirstName(firstName): Observable<any> {
    return this.http.get(url + `irecruit-api/resume/search-resume-by-firstName/${firstName}`)
  }
  // search dropdown by lastName
  getResumesAttachedByLastName(lastName): Observable<any> {
    return this.http.get(url + `irecruit-api/resume/search-resume-by-lastName/${lastName}`)
  }
  // search dropdown by domain
  getResumesAttachedByDomain(domain): Observable<any> {
    return this.http.get(url + `irecruit-api/resume/search-resume-by-domain/${domain}`)
  }
  // assign resume
  assignResumeToJD(resumes): Observable<any> {
    return this.http.post(url + `irecruit-api/jd/attach-resume`, resumes);
  }
  // download resumes
  getResumeDetailsFile(resumeId): Observable<any> {
    const httpOptions = {
      'responseType': 'arraybuffer' as 'json'
      //'responseType'  : 'blob' as 'json'        //This also worked
    };
    return this.http.get(url + `irecruit-api/resume/download/${resumeId}`,
      httpOptions)
  }
  errorHandler(respError: HttpErrorResponse | any) {
    if (respError.error instanceof ErrorEvent) {
      console.error('Client Side Error: ' + respError);
    }
    // else {
    //   console.error('Server Side Error: ' + respError);
    // }
    return throwError(respError || 'Server Downgrade Error');
  }
}
