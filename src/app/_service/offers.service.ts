import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { AlertService } from './alert.service';

const url = environment.url;

@Injectable({
  providedIn: 'root'
})
export class OffersService {

  constructor(private http: HttpClient, public alertService: AlertService) { }

  getOffersInitDtails(jdId, resumeId, interviewId): Observable<any> {
    return this.http.get(url + `irecruit-api/interview/get-feedback-details/${jdId}/${resumeId}/${interviewId}`)
  }
  getOffers(): Observable<any> {
    return this.http.get(url + `irecruit-api/offer/get-offer`)
  }
  getOffersDetailsByResume(resumeId): Observable<any> {
    return this.http.get(url + `irecruit-api/offer/get-offer-by-resume/${resumeId}`)
  }
  getInterviewFeedback(interviewId): Observable<any> {
    return this.http.get(url + `irecruit-api/interview/get-feedback-by-interview/${interviewId}`)
  }
  setInterviewFeedback(feedbackForm): Observable<any> {
    return this.http.post(url + 'irecruit-api/interview/feedback', feedbackForm);
  }
  editJoinData(joiningId): Observable<any> {
    return this.http.post(url + `offer/get-joining-data/${joiningId}`, {})
  }
  //  get join data by jdId & resume
  getJoinData(jdId, resumeId): Observable<any> {
    return this.http.get(url + `irecruit-api/offer/get-joining-data/${jdId}/${resumeId}`);
  }
  // add joining data
  addJoin(result): Observable<any> {
    return this.http.post(url + 'irecruit-api/offer/add-joining-data', result);
  }
  //kamesh
  getReleaseOffer(jdId, resumeId): Observable<any> {
    return this.http.post(url + `irecruit-api/offer/release-offer/${jdId}/${resumeId}`, {});
  }
  //kamesh
  //Shantam
  setExternalFeedback(feedbackForm): Observable<any> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return  this.http.post(url + 'save-external-feedback-form', feedbackForm,{ headers: headers });
  }
 
}
